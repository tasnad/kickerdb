## Overview ##
This is a kicker (a.k.a. table soccer, a.k.a. foosball) database for medium sized groups (like coworkers or a bunch of friends).
It tracks games and computes Élő-style scores similar to chess and table tennis.

## Inatallation ##
Make sure your webserver does not serve .git directories. For apache:
```
<DirectoryMatch "^/.*/\.git/">
    Require all denied
</DirectoryMatch>
```
Clone the repo into a subfolder of your web host.
Of course, you can also deploy it differently, if you wish.



Copy template_config.php to config.php and change at least:
* Set up KICKER_SECRET, KICKER_ADMIN_TOKEN. Use good passwords, since they are the only security measure.
* Write the mysql database password (what you have substituted for <*secret-password*> below) into KICKER_DB_PW
* SITE_TLD: where you host the app
* SUB_PATH: the subfolder where you installed the app (can also be empty: "/")
* If using mysql, create the database and user. You can use this (adjust the password before!):
```
CREATE DATABASE kickerdb;
CREATE USER kickerdb IDENTIFIED BY '<*secret-password*>';
GRANT SELECT, INSERT, UPDATE, CREATE, ALTER ON kickerdb.* TO kickerdb;
```
* DB_TYPE, DB_PATH: database settings (mysql or sqlite and e.g. /run/mysqld/mysqld.sock)
* DB_NAME, DB_USER: database name and user name, need to be the same as in the sql statement above.
* Call "cli/createTables.php" from the command line.

* To be able to send e-mails, you have two options: a) use the system sendmail binary, b) set mail settings here.
    - For both, set MAIL_TX_ADDRESS.
    - For option a) Comment all the defines SMTP_HOST, SMTP_PORT, SMTP_USER, SMTP_PASSWORD and set up sendmail for the user running php.
    - For option b) Set SMTP_HOST, SMTP_PORT, SMTP_USER, SMTP_PASSWORD and run `composer update` in the kickerdb/ folder.



## Usage ##
Open the app in a browser as: https://example.com/kickerdb/leaderboard.php?secret=*KICKER_SECRET*&admin_token=*KICKER_ADMIN_TOKEN*, go to the admin page (orange key) and start adding players.
Obviously, non-admin users should not have the admin_token, so their link is https://example.com/kickerdb?secret=*KICKER_SECRET*. They can see all pages, except the admin page.


## Local Debugging ##
* Settings in config.php:
```
define("KICKER_SECRET",      'a');
define("KICKER_ADMIN_TOKEN", 'b');
define("KICKER_DB_PW",       '-');
define("SITE_TLD", "localhost");
define("SUB_PATH", "/");
define("DB_TYPE", "sqlite"); define("DB_PATH", "/tmp/kicker.sql3");
```
* Either enable and use xdebug, or inspect stuff with psysh.
* psysh: Set a breakpoint with `require('/bin/psysh'); extract(\Psy\debug(get_defined_vars()));` Note that breakpoint does not mean you can step through the code, just examine stuff.
* xdebug: Install e.g. VSCode plugin 
* php.ini:
```
xdebug.mode = debug
xdebug.start_with_request = 1
xdebug.client_port = 9000
xdebug.remote_host = 127.0.0.1
xdebug.remote_port = 9000
```
* start debugging, set a breakpoint
* You can start a local php server to be able to browse the app: go to the base folder and php -S localhost:8080