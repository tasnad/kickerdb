<?php
define("WEBSITE_PHP_DEF", true);
require_once("common.php");
checkAccess();
setCookiesFromUrl();
require_once("apiFunctions.php");
?>

<!DOCTYPE html>
<html lang="en-US">

<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
<link rel="shortcut icon" type="image/x-icon" href="pics/favicon.ico" />

<link rel="stylesheet" href="css/main.css">
<link rel="stylesheet" href="css/fancySelect.css">
<?php loadTheme() ?>

<script src="lib/kicker.js"></script>
<script src="lib/moment.js"></script>
<script src="lib/fancySelect.js"></script>
</head>



<body>
<menu id="topnav"> </menu>

<div class="flex-container">
    <div> <h1>Add New Game</h1> </div>

    <div> <label> Single Game <input type="checkbox" id="singleGame" onchange="toggleSingleGame(this)"> </label> </div>

    <div> <fieldset class="entryFieldGrid">
        <legend>Team A</legend>
        <div class="grid">
            <label for="playerA1" id="lblPlayerA1"> Attack </label>
            <div id="playerA1"></div>
            <label for="playerA2" id="lblPlayerA2"> Defense </label>
            <div id="playerA2"></div>
            <label for="scoreA"> Goals </label>
            <div id="scoreA"></div>
            <label for="eloPreviewA"> Élő / Inv. </label>
            <div class="eloPreview" id="eloPreviewA"> </div>
            <button id="btnSwapA" onclick="swapPlayers('A')">Swap Players</button>
        </div>
    </fieldset> </div>

    <div> <fieldset class="entryFieldGrid">
        <legend>Team B</legend>
        <div class="grid">
            <label for="playerB1" id="lblPlayerB1"> Attack </label>
            <div id="playerB1"></div>
            <label for="playerB2" id="lblPlayerB2"> Defense </label>
            <div id="playerB2"></div>
            <label for="scoreB"> Goals </label>
            <div id="scoreB"></div>
            <label for="eloPreviewB"> Élő / Inv. </label>
            <div class="eloPreview" id="eloPreviewB"> </div>
            <button id="btnSwapB" onclick="swapPlayers('B')">Swap Players</button>
        </div>
    </fieldset> </div>


    <div> <label for="txtComment"> Comment </label> </div>
    <div> <input type="text" id="txtComment" size="25"> </input> </div>

    <div> <button id="btnAdd" onclick="addGame()"><b>Add Game</b></button> </div>

    <div> <h1>Games Today</h1> </div>
    <div> <h3 id=noGamesToday>No Games Today.</h3> </div>
    <div> <table class="dataTable padCells" id="tblToday"></table> </div>

    <div> <h1>Games This Week</h1> </div>
    <div> <h3 id=noGamesWeek>No Games This Week.</h3> </div>
    <div> <table class="dataTable padCells" id="tblWeek"></table> </div>
</div>



<script defer>
// swaps the players of the given team, i.e. A1 <-> A2
// teamName: "A" or "B"
function swapPlayers(teamName) {
    let p1 = document.getElementById("player" + teamName.toUpperCase() + "1");
    let p2 = document.getElementById("player" + teamName.toUpperCase() + "2");
    let p1Selected = fancySelect.get(p1);
    fancySelect.selectByText(p1, fancySelect.get(p2));
    fancySelect.selectByText(p2, p1Selected);
}



// toggles if a single game should be uploaded
function toggleSingleGame(theCheckBox){
    let a2   = document.getElementById("playerA2");
    let la1  = document.getElementById("lblPlayerA1");
    let la2  = document.getElementById("lblPlayerA2");
    let b2   = document.getElementById("playerB2");
    let lb1  = document.getElementById("lblPlayerB1");
    let lb2  = document.getElementById("lblPlayerB2");
    let btnA = document.getElementById("btnSwapA");
    let btnB = document.getElementById("btnSwapB");

    if (theCheckBox.checked) {
        a2.hidden = true;
        la2.hidden = true;
        b2.hidden = true;
        lb2.hidden = true;
        btnA.style = "display: none;";
        btnB.style = "display: none;";
        la1.innerText = "Player";
        lb1.innerText = "Player";
        wireSelects({double: false});
    }
    else {
        a2.hidden = false;
        la2.hidden = false;
        b2.hidden = false;
        lb2.hidden = false;
        btnA.style = "";
        btnB.style = "";
        la1.innerText = "Attack";
        lb1.innerText = "Attack";
        wireSelects({double: true});
    }

    updateEloPreview();
}



// Fills the player selectors
function setupSelectors(ctx) {
    // pre-select random names for all player selectors
    // get all active players, exclude names which were given as URL parameters
    let randomPlayerNames = JSON.parse( <?php print("'" . json_encode(getActivePlayerNames()) . "'"); ?> );
    for (let playerSel of ["playerA1", "playerA2", "playerB1", "playerB2"]) {
        // do not use names which are given in the url or in the cookie
        let plName = ctx.args[playerSel];
        if (plName !== undefined)
            randomPlayerNames = randomPlayerNames.filter(e => e !== plName);
        plName = getCookie("player");
        if (plName.length > 0)
            randomPlayerNames = randomPlayerNames.filter(e => e !== plName);
    }

    let allPlayers = JSON.parse( <?php print("'" . json_encode(getPlayerNames()) . "'"); ?> );
    // create and populate player selectors
    // also pre-select names from: URL, cookie, localStorage, random name
    // loop over current and next element
    for (let playerSel of ["playerA1", "playerA2", "playerB1", "playerB2"]) {
        // try from URL
        let preSelName = ctx.args[playerSel];
        if (preSelName === undefined) preSelName = "";

        // try to load from cookie (only if this is player A1)
        if (preSelName.length == 0 && playerSel === "playerA1")
            preSelName = getCookie("player");

        // try to read from localStorage
        if (preSelName.length == 0) {
            if (ctx.localStorageAvailable && localStorage[playerSel] !== undefined)
                preSelName = localStorage[playerSel];
        }

        // if no name was found, use a random one
        if (preSelName.length == 0)
            preSelName = randomPlayerNames.splice(Math.floor(Math.random() * randomPlayerNames.length), 1)[0];

        // create the current player selector and add all names
        fancySelect.create(document.getElementById(playerSel));
        fancySelect.add(document.getElementById(playerSel), allPlayers.map(x => capName(x)));
        fancySelect.selectByText(document.getElementById(playerSel), capName(preSelName));
    }

    // create score selectors
    let possibleScores = [...Array(30+1).keys()];
    for (let scoreSel of ["scoreA", "scoreB"]) {
        fancySelect.create(document.getElementById(scoreSel));
        fancySelect.add(document.getElementById(scoreSel), possibleScores);
    }
    fancySelect.selectByText(document.getElementById("scoreA"), "10");
    fancySelect.selectByText(document.getElementById("scoreB"), "7");

    // connect the selectors, default is double game
    wireSelects({double: true});

    // try to load other settings from localStorage
    if (ctx.localStorageAvailable) {
        if (localStorage["scoreA"])  fancySelect.selectByText(document.getElementById("scoreA"), localStorage["scoreA"]);
        if (localStorage["scoreB"])  fancySelect.selectByText(document.getElementById("scoreB"), localStorage["scoreB"]);
        if (localStorage["comment"]) document.getElementById("txtComment").value = localStorage["comment"];
    }
}

// connects selectors (if one closes, open the next), dependin on whether it's a single game or not
// and update elo preview
function wireSelects({double}) {
    let openChain;
    if (double)
        openChain = ["playerA1", "playerA2", "scoreA", "playerB1", "playerB2", "scoreB"];
    else
        openChain = ["playerA1", "scoreA", "playerB1", "scoreB"];

    for(let i = 0; i < openChain.length; i++) {
        // a small timeout is needed or the mousedown event of closing the current menu will also close the next in the chain
        fancySelect.setOnSelect(
            document.getElementById(openChain[i]),
            function() {
                // open next selector
                if (i < openChain.length - 1) {
                    setTimeout(
                        function() {
                            fancySelect.open(document.getElementById(openChain[i+1])); // open the next menu ...
                        },
                        10 // ... after a delay of 10ms
                    );
                }

                // elo preview update if something changed
                updateEloPreview();
            }
        );
    }
}



// updates the elo preview from all currently selected values
function updateEloPreview() {
    let playerA1 = fancySelect.get(document.getElementById("playerA1")).toLowerCase();
    let playerA2 = fancySelect.get(document.getElementById("playerA2")).toLowerCase();
    let playerB1 = fancySelect.get(document.getElementById("playerB1")).toLowerCase();
    let playerB2 = fancySelect.get(document.getElementById("playerB2")).toLowerCase();
    let scoreA   = fancySelect.get(document.getElementById("scoreA"));
    let scoreB   = fancySelect.get(document.getElementById("scoreB"));
    if (document.getElementById("singleGame").checked) {
        playerA2 = playerA1;
        playerB2 = playerB1;
    }
    getEloPreview(
        ctx,
        playerA1, playerA2, playerB1, playerB2,
        scoreA, scoreB
    ).then( function(gameElo) {
        let eloPreviewA = document.getElementById("eloPreviewA");
        let eloPreviewB = document.getElementById("eloPreviewB");
        eloPreviewA.innerText = `${gameElo.eloA.toFixed(1)} / ${gameElo.eloAInv.toFixed(1)}`;
        eloPreviewB.innerText = `${gameElo.eloB.toFixed(1)} / ${gameElo.eloBInv.toFixed(1)}`;
    });
}



// Fills today's games table if there are games or shows a message instead.
function updateGamesTable(ctx) {
    fetchData(["config", "gamesToday", "gamesWeek"])
    .then( function(data) {
        let noGamesToday = document.getElementById("noGamesToday");
        let noGamesWeek = document.getElementById("noGamesWeek");
        if (data.nGamesToday == 0) {
            tblToday.hidden = true;
            noGamesToday.hidden = false;
        }
        else {
            gamesTable(tblToday, data.gamesToday, data.config);
            tblToday.hidden = false;
            noGamesToday.hidden = true;
        }
        if (data.nGamesWeek == 0) {
            tblWeek.hidden = true;
            noGamesWeek.hidden = false;
        }
        else {
            gamesTable(tblWeek, data.gamesWeek, data.config);
            tblWeek.hidden = false;
            noGamesWeek.hidden = true;
        }
    }).catch( function(e) {
        alert(e);
    });
}



// fill the page if it has loaded
window.onload = function() {
    setupTopNav();

    ctx = getCtx();
    setupSelectors(ctx);
    updateGamesTable(ctx);
    updateEloPreview();
}



// uploads the game to the server
function addGame() {
    let ctx = getCtx();

    let playerA1 = fancySelect.get(document.getElementById("playerA1")).toLowerCase();
    let playerA2 = fancySelect.get(document.getElementById("playerA2")).toLowerCase();
    let playerB1 = fancySelect.get(document.getElementById("playerB1")).toLowerCase();
    let playerB2 = fancySelect.get(document.getElementById("playerB2")).toLowerCase();
    let scoreA   = fancySelect.get(document.getElementById("scoreA"));
    let scoreB   = fancySelect.get(document.getElementById("scoreB"));
    let comment  = document.getElementById("txtComment").value;

    // store selections in localStorage
    if (ctx.localStorageAvailable) {
        localStorage['playerA1'] = playerA1;
        localStorage['playerA2'] = playerA2;
        localStorage['playerB1'] = playerB1;
        localStorage['playerB2'] = playerB2;
        localStorage['scoreA']   = scoreA;
        localStorage['scoreB']   = scoreB;
        localStorage['comment']  = comment;
    }

    // if it is a single game, override second players now
    if (document.getElementById("singleGame").checked) {
        playerA2 = playerA1;
        playerB2 = playerB1;
    }

    // check that no player is in both teams, except dummies
    let dummyPlayerNames = JSON.parse( <?php print("'" . json_encode(getDummyPlayerNames()) . "'"); ?> );
    if (
        (!dummyPlayerNames.includes(playerA1) && playerA1 === playerB1) ||
        (!dummyPlayerNames.includes(playerA1) && playerA1 === playerB2) ||
        (!dummyPlayerNames.includes(playerA2) && playerA2 === playerB1) ||
        (!dummyPlayerNames.includes(playerA2) && playerA2 === playerB2)
    ) {
        alert("Illegal player configuration!");
        return;
    }

    // check that one team won and there was no draw
    if (scoreA === scoreB) {
        alert("Illegal scores, one team needs to win - go play another ball!");
        return;
    }
    if (!confirm("Really upload\n\n******************************\n" +
        capName(playerA1) + ", " + capName(playerA2) + "  " + scoreA + " : " + scoreB + "  " + capName(playerB1) + ", " + capName(playerB2) + "\n" +
        "******************************\n\nto the server?\n" +
        "\nTo delete a game, please contact an admin.")) {
        return;
    }

    let url = new URL(ctx.baseUrl + "/api.php");
    let urlParams = {
        op:       "add_game",
        playerA1: playerA1,
        playerA2: playerA2,
        playerB1: playerB1,
        playerB2: playerB2,
        scoreA:   scoreA,
        scoreB:   scoreB,
        comment:  comment,
    };
    Object.keys(urlParams).forEach(key => url.searchParams.append(key, urlParams[key]));

    fetch(url).then( function(res) {
        // convert response to JSON or throw error if access was denied.
        if (!res.ok) {
            throw Error(res.status + res.statusText);
        }
        return res.json()
    }).then( function(res) {
        let b = document.getElementById("btnAdd");
        b.disabled = true;
        b.innerText = "Please wait (3...)";
        updateGamesTable(ctx);
        updateEloPreview();

        window.setTimeout(function () {
            b.innerText = "Please wait (2...)";
            window.setTimeout(function () {
                b.innerText = "Please wait (1...)";
                window.setTimeout(function () {
                    b.innerText = "Add Game";
                    b.disabled = false;
                }, 1000);
            }, 1000);
        }, 1000);
    })
    .catch( function(e) {
        alert("Error storing the game! (" + e + ")");
    });
}
</script>
</body>
</html>
