<?php
define("WEBSITE_PHP_DEF", true);
require_once("common.php");
checkAccess();
checkAdminAccess();
setCookiesFromUrl();
require_once("apiFunctions.php");
?>

<!DOCTYPE html>
<html lang="en-US">

<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
<link rel="shortcut icon" type="image/x-icon" href="pics/favicon.ico" />

<link rel="stylesheet" href="css/main.css">
<link rel="stylesheet" href="css/fancySelect.css">
<?php loadTheme() ?>

<script src="lib/kicker.js"></script>

<script src="lib/moment.js"></script>
<script src="lib/moment-de.js"></script>
</head>



<body>
<menu id="topnav"> </menu>

<div class="flex-container">
    <div> <h1>Add Player</h1> </div>
    <div> <label for="txtName"> Name </label> <input type="text" id="txtName" size="25"> </input> </div>
    <div> <label for="txtMail"> E-mail </label> <input type="text" id="txtMail" size="40"> </input> </div>
    <div> <label> Newcomer Bonus (max points per game in percent, 0..200) <input type="number" id="newcomerBonus" step="1" min="0" max="200" value="100"> </label> </div>
    <div> <label> Guest <input type="checkbox" id="guest"> </label> </div>
    <div> <button id="btnAdd" onclick="addPlayer()"><b>Add Player</b></button> </div>

    <div> <h1>Update Élő</h1> </div>
    <div> <button id="btnUpdateElo" onclick="updateElo()"><b>Update</b></button> </div>

    <div> <h1>Reset Player Élő values</h1> </div>
    <div> <h3 >Don't worry, these values are recreated every time a game is added or with the Update button above.</h3> </div>
    <div> <button id="btnResetEloValues" onclick="resetPlayerEloValues()"><b>Reset</b></button> </div>

    <div> <h1>Ignored Games</h1> </div>
    <div> <label for="txtGameId"> Game ID </label> <input type="text" id="txtGameId" size="5"> </input> </div>
    <div>
        <button onclick="setGameActive(false)"><b>Ignore Game</b></button>
        <button onclick="setGameActive(true)"> <b>Use Game</b>   </button>
    </div>
    <div> <table class="dataTable" id="tblGames"></table> </div>
</div>



<script defer>
// Fetches all games and fills the games table.
function loadGames() {
    fetchData(["config", "allGames", "includeIgnoredGames", "includeOldGames"]).then( function(ret) {
        gamesTable(document.getElementById("tblGames"), ret.allGames, ret.config, ()=>"", true, true, false);
    });
}

window.onload = function() {
    setupTopNav();
    loadGames();

    // disable the bonus for guests
    document.getElementById("guest").onchange = function() {
        let bonusInp = document.getElementById("newcomerBonus");
        bonusInp.disabled = this.checked;
        bonusInp.value = 100;
    };
}



// uploads the player to the server
function addPlayer() {
    let playerName = document.getElementById("txtName").value;
    let playerMail = document.getElementById("txtMail").value;
    if (document.getElementById("guest").checked) guest = "1"; else guest = "0";
    let newcomerBonus = document.getElementById("newcomerBonus").value;

    if (playerName.length == 0) {
        alert("Can't add player with empty name!");
        return;
    }
    let ctx = getCtx();
    let url = new URL(ctx.baseUrl + "/api.php");
    let urlParams = {
        op:    "add_player",
        name:  playerName,
        guest: guest,
        email: playerMail,
        newcomerBonus: newcomerBonus,
    };
    Object.keys(urlParams).forEach(key => url.searchParams.append(key, urlParams[key]));

    fetch(url).then( function(res) {
        // convert response to JSON or throw error if access was denied.
        if (!res.ok) {
            throw Error(res.status + res.statusText);
        }
        return res.json()
    }).then( function(res) {
        alert("Player added.");
    })
    .catch( function(e) {
        alert("Error storing the player! (" + e + ")");
    });
}



// updates the elo scores
function updateElo() {
    let ctx = getCtx();
    let url = new URL(ctx.baseUrl + "/api.php");
    url.searchParams.append("op", "update_elo")

    fetch(url).then( function(res) {
        // convert response to JSON or throw error if access was denied.
        if (!res.ok) {
            throw Error(res.status + res.statusText);
        }
        return res.json()
    }).then( function(res) {
        alert("Élő updated.");
    })
    .catch( function(e) {
        alert("Error updating Élő! (" + e + ")");
    });
}



// updates the elo scores
function resetPlayerEloValues() {
    let ctx = getCtx();
    let url = new URL(ctx.baseUrl + "/api.php");
    url.searchParams.append("op", "reset_player_elo_values")

    fetch(url).then( function(res) {
        // convert response to JSON or throw error if access was denied.
        if (!res.ok) {
            throw Error(res.status + res.statusText);
        }
        return res.json()
    }).then( function(res) {
        alert("Player Élő values reset.");
    })
    .catch( function(e) {
        alert("Error resetting Élő values! (" + e + ")");
    });
}



// updates the elo scores
function setGameActive(active) {
    let gameId = document.getElementById("txtGameId").value;
    let activeStr = "0"; if (active) activeStr = "1";

    let ctx = getCtx();
    let url = new URL(ctx.baseUrl + "/api.php");
    let urlParams = {
        op: "set_game_active",
        id: gameId,
        active: activeStr
    };
    Object.keys(urlParams).forEach(key => url.searchParams.append(key, urlParams[key]));

    fetch(url).then( function(res) {
        // convert response to JSON or throw error if access was denied.
        if (!res.ok) {
            throw Error(res.status + res.statusText);
        }
        return res.json()
    }).then( function(res) {
        if (active)
            alert("Game activated.");
        else
            alert("Game deactivated.");
        loadGames();
    })
    .catch( function(e) {
        alert("Error changing the game's state! (" + e + ")");
    });
}
</script>
</body>
</html>
