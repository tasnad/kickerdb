<?php
define("WEBSITE_PHP_DEF", true);
require_once("common.php");
checkAccess();
setCookiesFromUrl();
?>

<!DOCTYPE html>
<html lang="en-US">

<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
<link rel="shortcut icon" type="image/x-icon" href="pics/favicon.ico" />

<link rel="stylesheet" href="css/main.css">
<?php loadTheme() ?>

<script src="lib/kicker.js"></script>
<script src="lib/moment.js"></script>
</head>



<body>
<menu id="topnav"> </menu>

<div class="flex-container">
    <div> <h1 id="infoTxt1"></h1> </div>
    <div> <table class="dataTable" id="tbl1"></table> </div>

    <div> <h1 id="infoTxt2"></h1> </div>
    <div> <table class="dataTable" id="tbl2"></table> </div>

    <div> <h1 id="infoTxt3"></h1> </div>
    <div> <table class="dataTable" id="tbl3"></table> </div>

    <div> <h1 id="infoTxt4"></h1> </div>
    <div> <table class="dataTable" id="tbl4"></table> </div>
</div>



<script defer>
window.onload = function() {
    setupTopNav();
    fetchData(["config", "singleGames", "doubleGames", "gamesToday", "gamesWeek"]).then( function(data) {
        infoTxt1.innerText = "Games Today (" + data.nGamesToday + ")";
        gamesTable(tbl1, data.gamesToday, data.config);

        infoTxt2.innerText = "Games This Week (" + data.nGamesWeek + ")";
        gamesTable(tbl2, data.gamesWeek, data.config);

        infoTxt3.innerText = "All Single Games (" + data.nSingleGames + ")";
        gamesTable(tbl3, data.singleGames, data.config);

        infoTxt4.innerText = "All Double Games (" + data.nDoubleGames + ")";
        gamesTable(tbl4, data.doubleGames, data.config);
    });
}
</script>
</body>
</html>
