<?php
// This is the API to interface the kicker database.
// The format is always
//   <base url>/api.php?secret=<secret>&op=<operation>[&<operation parameters]
// where
// <base url> is the gives the location to this file
// <secret> is the kicker secret token
// <operation> is the API request selection, see below (e.g. "get_players")
// <operation parameters> are required by some API requests (e.g. "player" for "get_my_score") and optional for others
//
// The API relies on different cookies:
// secret: needed to access all sites
// admin_token: needed to access special admin APIs (e.g. add a player)
// storePlayer: simply stores the given player name as a cookie
//
// Available API operations:
//
// *** add_game ***
// required parameters:
//   playerA1, playerA2, playerB1, playerB2 (player IDs)
//   scoreA, scoreB (goals by team A and B)
//   comment
// optional parameters: none
// output: none
//
// *** update_elo ***
// Recomputes the Élő points for all players. This is also automatically called after a game is added.
// required parameters: none
// optional parameters: none
// output: none
//
// *** get_game_elo ***
// Returns how much Élő a game with this settings would lead to.
// required parameters: playerA1 playerA2 playerB1 playerB2 scoreA scoreB
// optional parameters: none
// output: dict with elo data, e.g. {"eloA":4.416426262930954,"eloB":-4.416426262930954,"eloInv":5.33940796417233,"K":14}
//
// *** get_data ***
// required parameters:
//   queries: selects what data to return. For choices see below. Give as a comma separated list, e.g. "doubleGames,singleGames"
// optional parameters:
//   rankSortKey: Sort key for the rankings ("id", "gamesWon", "gamesLost", "gamesTotal", "score", "winRatio", "efficiency", "eloTotal"), default is eloTotal.
//   timeZone: Used to determine start of the day and week, default is set in config.php -> TIMEZONE.
// output: dict with selected data, i.e.:
//   allGames:      total played 2 on 2 and 2 on 1 games
//   doubleGames:   total played 2 on 2 or 2 on 1 games
//   singleGames:   total played 1 on 1 games
//   gamesToday:    games played today
//   gamesWeek:     games played this week (since monday)
//   allPlayers:    all players with stats (like in get_players)
//   playersToday:  all players with games today
//   playersWeek:   all players with games this week
//   rankingToday:  ranking based on "rankSortKey", only today
//   rankingWeek:   ranking based on "rankSortKey", only this week
//   rankingActive: ranking based on most games played ("gamesTotal")
//   eloDetails:    Add eloHistory and eloPenalties to player lists. This has no effect if no player lists are queried
//
// *** get_games ***
// required parameters: none
// optional parameters: timeFrom, timeTo: only consider games between these dates, default is all games (UTC!)
// output: list of all played games
//
// *** get_player_games ***
// required parameters: player (name of the player whose games to return)
// optional parameters: timeFrom, timeTo: only consider games between these dates, default is all games (UTC!)
// output: list of games this player participated
//
// *** get_ranking ***
// required parameters: none
// optional parameters:
//   sortKey ("id", "gamesWon", "gamesLost", "gamesTotal", "score", "winRatio", "efficiency", "eloTotal"), default is eloTotal
//   timeFrom, timeTo: only consider games between these dates, default is all games (UTC!)
// output: sorted list players with ranking
//
// *** get_my_score ***
// required parameters: player (name of the players to return)
// optional parameters:
//   type ("gamesWon", "gamesLost", "gamesTotal", "score", "winRatio", "efficiency", "eloTotal"), default is eloTotal
//   timeFrom, timeTo: only consider games between these dates, default is all games (UTC!)
// output: player's score
//
// *** get_me ***
// required parameters: player (name of the players to return)
// optional parameters:
//   sortKey ("id", "gamesWon", "gamesLost", "gamesTotal", "score", "winRatio", "efficiency", "eloTotal"), default is eloTotal
//   timeFrom, timeTo: only consider games between these dates, default is all games (UTC!)
// output: all data of the selected player
//
//
// ****************************************
// Admin functions, access with admin_token
// ****************************************
// *** add_player ***
// required parameters: name
// optional parameters: guest (0 or 1), email, newcomerBonus
// output: none
//
// *** set_game_active ***
// required parameters: id, active (1 or 0)
// output: none
//
//
//
// The API always returns JSON, so add this header at the very top.
header('Content-Type: application/json');
define("WEBSITE_PHP_DEF", true);
require_once("common.php");
checkGuard(true);
checkAccess(true);
require_once("apiFunctions.php");


// load parameters from command line or from $_GET
if (!empty($argv[1]))
    parse_str($argv[1], $paramsIn);
else
    $paramsIn = $_GET;

// Perform API requests, depending on the "op" GET parameter.
if(isset($paramsIn["op"])) {
    if($paramsIn["op"] === "add_game") {
        if(isset($paramsIn["playerA1"])) $playerA1 = $paramsIn["playerA1"]; else dieBadReq("Missing player A1's name!");
        if(isset($paramsIn["playerA2"])) $playerA2 = $paramsIn["playerA2"]; else dieBadReq("Missing player A2's name!");
        if(isset($paramsIn["playerB1"])) $playerB1 = $paramsIn["playerB1"]; else dieBadReq("Missing player B1's name!");
        if(isset($paramsIn["playerB2"])) $playerB2 = $paramsIn["playerB2"]; else dieBadReq("Missing player B2's name!");
        if(isset($paramsIn["scoreA"]))   $scoreA   = $paramsIn["scoreA"];   else dieBadReq("Missing score A!");
        if(isset($paramsIn["scoreB"]))   $scoreB   = $paramsIn["scoreB"];   else dieBadReq("Missing score B!");
        $playTime = gmdate("Y-m-d H:i:s", time()); // UTC now
        if(isset($paramsIn["comment"]))  $comment  = $paramsIn["comment"];  else $comment = "";

        if ($scoreA < 0 || $scoreA > 99 || $scoreB < 0 || $scoreB > 99)
            dieBadReq("Wrong score!");
        if(addGame($playerA1, $playerA2, $playerB1, $playerB2, $scoreA, $scoreB, $playTime, $comment))
            print(json_encode("Game stored."));
        else
            dieBadReq("Error storing the game!");
    }
    elseif($paramsIn["op"] === "update_elo") {
        // trigger an update now, do not wait for a game to be added
        if (updateElo())
            print(json_encode("Elo points updated."));
        else
            dieBadReq("Error updating the Elo points!");
    }
    elseif($paramsIn["op"] === "reset_player_elo_values") {
        if (resetPlayerEloValues())
            print(json_encode("Elo related valued for all players were reset."));
        else
            dieBadReq("Error resetting the Elo related values!");
    }
    elseif($paramsIn["op"] === "get_game_elo") {
        // compute how much elo a game would change
        if(isset($paramsIn["playerA1"])) $playerA1Name = $paramsIn["playerA1"]; else dieBadReq("Missing player A1's name!");
        if(isset($paramsIn["playerA2"])) $playerA2Name = $paramsIn["playerA2"]; else dieBadReq("Missing player A2's name!");
        if(isset($paramsIn["playerB1"])) $playerB1Name = $paramsIn["playerB1"]; else dieBadReq("Missing player B1's name!");
        if(isset($paramsIn["playerB2"])) $playerB2Name = $paramsIn["playerB2"]; else dieBadReq("Missing player B2's name!");
        if(isset($paramsIn["scoreA"]))   $scoreA       = $paramsIn["scoreA"];   else dieBadReq("Missing score A!");
        if(isset($paramsIn["scoreB"]))   $scoreB       = $paramsIn["scoreB"];   else dieBadReq("Missing score B!");

        // get the list of all players and extract those we want
        $allPlayers = getPlayers();
        try {
            $gameElo = computeGameElo(
                [
                    "A1" => $allPlayers[$playerA1Name],
                    "A2" => $allPlayers[$playerA2Name],
                    "B1" => $allPlayers[$playerB1Name],
                    "B2" => $allPlayers[$playerB2Name]
                ],
                $scoreA, $scoreB
            );
        } catch (Exception $e) {
            dieBadReq("Error getting the elo score!");
        }
        print(json_encode($gameElo));
    }
    elseif($paramsIn["op"] === "get_data") {
        if ( isset($paramsIn["queries"]) && strlen($paramsIn["queries"]) > 0) {
            $queries = array_map('trim', explode(",", $paramsIn["queries"]));
            $possibleQueries = array(
                "config",
                "allGames", "doubleGames", "singleGames", "gamesToday", "gamesWeek",
                "allPlayers", "playersWeek", "playersToday",
                "rankingToday", "rankingWeek", "rankingActive",
                "eloDetails", "includeIgnoredGames", "includeOldGames"
            );
            foreach ($queries as $q) {
                if (!in_array($q, $possibleQueries))
                    dieBadReq("Invalid data queries!");
            }
        }
        else {
            dieBadReq("Missing query!");
        }

        if (isset($paramsIn["rankSortkey"])) {
            $rankSortKey = $paramsIn["rankSortkey"];
            $sortKeys = array("id", "gamesWon", "gamesLost", "gamesTotal", "score", "winRatio", "efficiency", "eloTotal");
            if (!in_array($rankSortKey, $sortKeys))
                dieBadReq("Invalid sortkey!");
        }
        else {
            $rankSortKey = "eloTotal";
        }

        if (isset($paramsIn["timeZone"]))
            $timeZone = $paramsIn["timeZone"];
        else
            $timeZone = TIMEZONE;

        $startOfDay = DateTime::createFromFormat("Y-m-d H:i:s", date("Y-m-d 00:00:00"), new DateTimeZone($timeZone));
        $startOfDay->setTimeZone(new DateTimeZone("UTC"));
        $startOfDay = $startOfDay->format("Y-m-d H:i:s");

        $startOfWeek = DateTime::createFromFormat("Y-m-d H:i:s", date("Y-m-d 00:00:00", strtotime("monday this week")), new DateTimeZone($timeZone));
        $startOfWeek->setTimeZone(new DateTimeZone("UTC"));
        $startOfWeek = $startOfWeek->format("Y-m-d H:i:s");

        // compute query results
        $doubleGames   = NULL;
        $singleGames   = NULL;
        $gamesToday    = NULL;
        $gamesWeek     = NULL;
        $allPlayers    = NULL;
        $playersToday  = NULL;
        $playersWeek   = NULL;
        $rankingToday  = NULL;
        $rankingWeek   = NULL;
        $rankingActive = NULL;

        if (in_array("config", $queries)) {
            $config = array(
                "time_window"                => TIME_WINDOW,
                "playerInactiveTime"         => PLAYER_INACTIVE_TIME,
                "eloInit"                    => ELO_INIT,
                "eloF"                       => ELO_F,
                "eloKRegular"                => ELO_K_REGULAR,
                "eloNewcomerGames"           => ELO_NEWCOMER_GAMES,
                "goals_per_game"             => GOALS_PER_GAME,
                "elo_excess_goals_max"       => ELO_EXCESS_GOALS_MAX,
                "elo_s_exponent"             => ELO_S_EXPONENT,
                "eloPenaltyDelayDays"        => ELO_PENALTY_DELAY_DAYS,
                "eloPenaltyPerDay"           => ELO_PENALTY_PER_DAY,
                "eloPenaltyExponent"         => ELO_PENALTY_EXPONENT,
                "eloPenaltyActiveRange"      => ELO_PENALTY_ACTIVE_RANGE,
                "eloPenaltyDecayFractPerDay" => ELO_PENALTY_DECAY_FRACT_PER_DAY,
            );
        } else {
            $config = [];
        }

        if (in_array("eloDetails",          $queries)) $includeEloDetails   = true; else $includeEloDetails   = false;
        if (in_array("includeIgnoredGames", $queries)) $includeIgnoredGames = true; else $includeIgnoredGames = false;
        if (in_array("includeOldGames",     $queries)) $includeOldGames     = true; else $includeOldGames     = false;

        // load players and games only if requested
        if ( in_array("allGames", $queries)     || in_array("allPlayers", $queries)  ||
             in_array("doubleGames", $queries)  || in_array("singleGames", $queries) ||
             in_array("playersToday", $queries) || in_array("playersWeek", $queries)
        ) {
            $allGames = getGames(null, null, false, null, false, $includeIgnoredGames, $includeOldGames);
        }
        if (in_array("allPlayers",  $queries)) $allPlayers  = getPlayers($allGames, $includeEloDetails);
        if (in_array("doubleGames", $queries)) $doubleGames = filterForSingleOrDoubleGames($allGames, true);
        if (in_array("singleGames", $queries)) $singleGames = filterForSingleOrDoubleGames($allGames, false);

        // we need todays games if they are requested, and for the ranking
        if ( in_array("gamesToday", $queries) || in_array("rankingToday", $queries) )
            $gamesToday = getGames($startOfDay, null, false, null, false, $includeIgnoredGames, $includeOldGames);
        // similar for this weeks games
        if ( in_array("gamesWeek", $queries) || in_array("rankingWeek", $queries) || in_array("rankingActive", $queries))
            $gamesWeek = getGames($startOfWeek, null, false, null, false, $includeIgnoredGames, $includeOldGames);

        // list of today's and this week's players
        if (in_array("playersToday", $queries))
            $playersToday = filterPlayersByLastGame(getPlayers($allGames, $includeEloDetails), $startOfDay);
        if (in_array("playersWeek",  $queries))
            $playersWeek  = filterPlayersByLastGame(getPlayers($allGames, $includeEloDetails), $startOfWeek);

        // assemble rankings
        // get player lists where only some games account for stats
        if (in_array("rankingToday",  $queries))
            $playersGamesOnlyToday = getPlayers($gamesToday, $includeEloDetails);
        if ( in_array("rankingWeek",  $queries) || in_array("rankingActive", $queries) )
            $playersGamesOnlyWeek  = getPlayers($gamesWeek,  $includeEloDetails);
        // get the rankings
        if (in_array("rankingToday",  $queries)) $rankingToday  = getRanking($rankSortKey, $playersGamesOnlyToday);
        if (in_array("rankingWeek",   $queries)) $rankingWeek   = getRanking($rankSortKey, $playersGamesOnlyWeek);
        if (in_array("rankingActive", $queries)) $rankingActive = getRanking("gamesTotal", $playersGamesOnlyWeek);

        // delete intermediate results if they are not requested
        if (!in_array("allGames",     $queries)) $allGames   = NULL;
        if (!in_array("gamesToday",   $queries)) $gamesToday = NULL;
        if (!in_array("gamesWeek",    $queries)) $gamesWeek  = NULL;

        // print the results
        print(json_encode(array(
            "config"        => $config,
            "allGames"      => $allGames,
            "doubleGames"   => $doubleGames,
            "singleGames"   => $singleGames,
            "gamesToday"    => $gamesToday,
            "gamesWeek"     => $gamesWeek,
            "allPlayers"    => $allPlayers,
            "playersToday"  => $playersToday,
            "playersWeek"   => $playersWeek,
            "rankingToday"  => $rankingToday,
            "rankingWeek"   => $rankingWeek,
            "rankingActive" => $rankingActive,
        )));
    }
    elseif($paramsIn["op"] === "get_games") {
        if (isset($paramsIn["timeFrom"])) $timeFrom = $paramsIn["timeFrom"]; else $timeFrom = "0001-01-01 00:00:00";
        if (isset($paramsIn["timeTo"]))   $timeTo   = $paramsIn["timeTo"];   else $timeTo   = "9999-12-31 23:59:59";

        $games = getGames($timeFrom, $timeTo);
        if ($games === NULL) dieBadReq("Could not find any games!");
        print(json_encode($games));
    }
    elseif($paramsIn["op"] === "get_player_games") {
        if (!isset($paramsIn["player"])) dieBadReq("Missing player name!");
        $pName = $paramsIn["player"];

        if (isset($paramsIn["timeFrom"])) $timeFrom = $paramsIn["timeFrom"]; else $timeFrom = "0001-01-01 00:00:00";
        if (isset($paramsIn["timeTo"]))   $timeTo   = $paramsIn["timeTo"];   else $timeTo   = "9999-12-31 23:59:59";

        $games = getGames($timeFrom, $timeTo, false, $pName);
        if ($games === NULL)
            dieBadReq("Could not find any games!");
        print(json_encode($games));
    }
    elseif($paramsIn["op"] === "get_ranking") {
        if (isset($paramsIn["sortkey"])) {
            $sortKey = $paramsIn["sortkey"];
            $sortKeys = array("id", "gamesWon", "gamesLost", "gamesTotal", "score", "winRatio", "efficiency", "eloTotal");
            if (!in_array($sortKey, $sortKeys))
                dieBadReq("Invalid sortkey!");
        }
        else
            $sortKey = "eloTotal";

        if (isset($paramsIn["timeFrom"])) $timeFrom = $paramsIn["timeFrom"]; else $timeFrom = "0001-01-01 00:00:00";
        if (isset($paramsIn["timeTo"]))   $timeTo   = $paramsIn["timeTo"];   else $timeTo   = "9999-12-31 23:59:59";
        $ranking = getRanking($sortKey, getPlayers(getGames($timeFrom, $timeTo)));
        if ($ranking === NULL)
            dieBadReq("Could not get ranking!");
        else
            print(json_encode($ranking));
    }
    elseif($paramsIn["op"] === "get_my_score") {
        if (!isset($paramsIn["player"]))
            dieBadReq("Missing player name!");
        $pName = $paramsIn["player"];

        if (isset($paramsIn["type"])) {
            $type = $paramsIn["type"];
            $types = array("gamesWon", "gamesLost", "gamesTotal", "score", "winRatio", "efficiency", "eloTotal");
            if (!in_array($type, $types))
                dieBadReq("Invalid score type!");
        }
        else
            $type = "eloTotal";

        if (isset($paramsIn["timeFrom"])) $timeFrom = $paramsIn["timeFrom"]; else $timeFrom = "0001-01-01 00:00:00";
        if (isset($paramsIn["timeTo"]))   $timeTo   = $paramsIn["timeTo"];   else $timeTo   = "9999-12-31 23:59:59";
        $players = getPlayers(getGames($timeFrom, $timeTo));
        if (array_key_exists($pName, $players))
            print(json_encode($players[$pName][$type], JSON_FORCE_OBJECT));
        else
            dieBadReq("Player not found!");
    }
    elseif($paramsIn["op"] === "get_me") {
        if (!isset($paramsIn["player"]))
            dieBadReq("Missing player name!");
        $pName = $paramsIn["player"];

        if (isset($paramsIn["sortkey"])) {
            $sortKey = $paramsIn["sortkey"];
            $sortKeys = array("id", "gamesWon", "gamesLost", "gamesTotal", "score", "winRatio", "efficiency", "eloTotal");
            if (!in_array($sortKey, $sortKeys))
                dieBadReq("Invalid sortkey!");
        }
        else
            $sortKey = "eloTotal";

        if (isset($paramsIn["timeFrom"])) $timeFrom = $paramsIn["timeFrom"]; else $timeFrom = "0001-01-01 00:00:00";
        if (isset($paramsIn["timeTo"]))   $timeTo   = $paramsIn["timeTo"];   else $timeTo   = "9999-12-31 23:59:59";
        $ranking = getRanking($sortKey, getPlayers(getGames($timeFrom, $timeTo)));
        if ($ranking === NULL)
            dieBadReq("Could not get ranking!");

        $didPrint = false;
        foreach ($ranking as $p) {
            if ($p["name"] === $pName) {
                print(json_encode($p));
                $didPrint = true;
                break;
            }
        }
        if (!$didPrint)
            dieBadReq("Player not found!");
    }
    // *******************************************
    // ************ admin functions **************
    // *******************************************
    elseif($paramsIn["op"] === "add_player") {
        checkAdminAccess(true);
        if(isset($paramsIn["name"]))  $name  = $paramsIn["name"];            else dieBadReq("Missing player name!");
        if(isset($paramsIn["guest"])) $guest = ($paramsIn["guest"] === "1"); else $guest = false;
        if(isset($paramsIn["email"])) $email = $paramsIn["email"];           else $email = "";
        if(isset($paramsIn["newcomerBonus"]) && $paramsIn["newcomerBonus"] != "100") $newcomerBonus = floatval($paramsIn["newcomerBonus"]); else $newcomerBonus = null;

        if(addPlayer($name, $guest, $email, $newcomerBonus))
            print(json_encode("Player added."));
        else
            dieBadReq("Error adding player!");
    }
    elseif($paramsIn["op"] === "set_game_active") {
        checkAdminAccess(true);
        if(isset($paramsIn["id"]))                                $gameId = $paramsIn["id"]; else dieBadReq("Missing game ID!");
        if(isset($paramsIn["active"]) && $paramsIn["active"] === "1") $active = true; else $active = false;

        if(setGameActive($gameId, $active))
            print(json_encode("Game state changed."));
        else
            dieBadReq("Error changing the game's state!");
    }
    else {
        dieBadReq("Unknown operation '". $paramsIn["op"] . "'!");
    }
}
else {
    dieBadReq("No operation given!");
}
