from i3pystatus import IntervalModule
import psutil, os
from .core.util import round_dict


class Kickerstats(IntervalModule):
    """
    Shows kicker stats
     .. rubric:: Available formatters
    * {score}
    """

    format = "kicker: {score}"
    color = "#00FF00"
    alert_color = "#FF0000"
    alert_below = 0
    path = "~/.local/bin/get_player_stats.sh"

    settings = (
        ("format", "format string used for output."),
        ("alert_below", "maximal score for alert state"),
        ("color", "standard color"),
        ("alert_color",
         "defines the color used when alert percentage is exceeded"),
        ("path", "path to shell script that gets player stats"),
        #("player_name", "defines the name of the player"),
    )

    def run(self):
        score = int(os.popen(self.path).read().split(":")[1])

        if score < self.alert_below:
            color = self.alert_color
        else:
            color = self.color

        cdict = {
            "score": score,
        }

        self.data = cdict
        self.output = {
            "full_text": self.format.format(**cdict),
            "color": color
        }

