<?php
// The API functions are defined in this file, for the API itself, look in api.php.
require_once("common.php");
checkGuard();
checkAccess();
require_once("db.php");



// Returns when the time period to consider started.
// Checks if TIME_WINDOW starts with "P" and if yes, subtracts that ammount from now,
// otherwise uses TIME_WINDOW directly.
function getGamesStartTime() {
    if (substr( TIME_WINDOW, 0, 1 ) === "P") {
        $curTime = new DateTime(null, new DateTimeZone('UTC'));
        return $curTime->sub(new DateInterval(TIME_WINDOW));
    }
    else {
        return new DateTime(TIME_WINDOW, new DateTimeZone('UTC'));
    }
}



abstract class PlayerType
{
    const Regular = 0; // a regular player
    const Guest   = 1; // a guest which will likely play more than one game. Like a regular player, but no penalties
    const Dummy   = 2; // a special guest with no penalties, no elo and can be present in both teams (if there are two guests, for example)
}

abstract class EloPenaltyType
{
    const Active   = 0; // penalty is valid and should be used
    const TooShort = 1; // gap is not long enough
    const Decaying = 2; // gap is long ago and the penalty is in the decaying phase
    const Old      = 3; // gap is long ago, penalty has decayed
}

// Returns the penalty for a given inactivity period.
// Also returns the original penalty (pre decay) and the "type" of the penalty (see EloPenaltyType).
// $gapStart: DateTime object which holds when the gap started
// $gapEnd:   DateTime object which holds when the gap ended
// curTime:   DateTime object whith the current time
function getEloPenalty($gapStart, $gapEnd, $curTime) {
    // compute gap length in days
    $diffDays = ($gapEnd->getTimestamp() - $gapStart->getTimestamp()) / 86400; // convert seconds to days
    // see if the gap is long enough
    if ($diffDays <= ELO_PENALTY_DELAY_DAYS) {
        // gap too short, no penalty here
        return [0, 0, EloPenaltyType::TooShort];
    }
    else {
        // gap long enough, compute the penalty before decaying
        $penaltyLin    = ($diffDays - ELO_PENALTY_DELAY_DAYS) * ELO_PENALTY_PER_DAY;
        $penaltyPreDec = $penaltyLin ** ELO_PENALTY_EXPONENT;

        // see if the penalty is active, decaying or old
        $daysAgo = ($curTime->getTimestamp() - $gapEnd->getTimestamp()) / 86400; // convert seconds to days
        if ($daysAgo < ELO_PENALTY_ACTIVE_RANGE) {
            // still in the active region
            return [$penaltyPreDec, $penaltyPreDec, EloPenaltyType::Active];
        }
        else {
            // penalty over, see how much it is decayed
            $daysDecay = $daysAgo - ELO_PENALTY_ACTIVE_RANGE;
            $penaltyDec = $penaltyPreDec * (1 - ELO_PENALTY_DECAY_FRACT_PER_DAY) ** $daysDecay;
            if ($penaltyDec > 0.5) {
                return [$penaltyDec, $penaltyPreDec, EloPenaltyType::Decaying];
            }
            else {
                // cut off below 0.5 points
                return [0,           $penaltyPreDec, EloPenaltyType::Old];
            }
        }
    }
}

// Computes the elo outcome of a game, based on current player elos.
// Returns an array with eloA, eloB, eloAInv, eloBInv, K
function computeGameElo($players, $scoreA, $scoreB) {
    // to figure out K, we need to check if a player with active bonus was present in the game
    // and has has not too many games already
    // start with assuming INF and reduce it whenever we find a newcomer
    $K = INF;
    foreach ($players as $p) {
        if (
            ( $p["playerType"] === PlayerType::Regular ) // only regular players, no guests
            &&
            ( $p["newcomerBonusGranted"] === 1 ) // he has the bonus flag set
            &&
            ( $p["nGamesTotal"] <= ELO_NEWCOMER_GAMES ) // he has not played too many games
        ){
            // always use the lowest K to be fair to weaker players
            $K = min($K, ELO_K_REGULAR * $p["newcomerBonus"]/100.0);
        }
    }
    // if no newcomer was in the game, use the regular value
    if ($K == INF)
        $K = ELO_K_REGULAR;

    // compute expectation values
    // treat teams as virtual players with mean score. Also works for single games
    $eloDiffA = ($players["B1"]["eloTotal"] + $players["B2"]["eloTotal"])/2 - ($players["A1"]["eloTotal"] + $players["A2"]["eloTotal"])/2;
    // bind difference to -ELO_F .. ELO_F
    $eloDiffA = min(ELO_F, max(-ELO_F, $eloDiffA));
    // compute expectation
    $EA = 1 / (1 + 10 ** ($eloDiffA/ELO_F));
    // EB = 1 - EA;

    // We compute the "S" value according to the difference of goals.
    // In the chess Élő system, this is 1 for a win, 0.5 for a draw and 0 for a loss.
    // The largest goal difference is GOALS_PER_GAME and the smallest is 1 (golden goal).
    $goalDiffA = $scoreA - $scoreB;
    $SA = 0.5 + sign($goalDiffA) * 0.5 * (abs($goalDiffA)/GOALS_PER_GAME) ** ELO_S_EXPONENT;
    $SA = min(1, max(0, $SA)); // bind to 0..1
    // SB = 1 - SA
    // also compute SA if the goals were swapped (for eloAInv)
    $SAInv = 0.5 + sign(-$goalDiffA) * 0.5 * (abs($goalDiffA)/GOALS_PER_GAME) ** ELO_S_EXPONENT;
    $SAInv = min(1, max(0, $SAInv));

    // Reduce score for overly long games.
    // The reduction scales between 1 for a regular game and 0.5 for a game with GOALS_PER_GAME + ELO_EXCESS_GOALS_MAX or more goals.
    $excessGoals = max($scoreA, $scoreB) - GOALS_PER_GAME;
    if (ELO_EXCESS_GOALS_MAX > 0 && $excessGoals > 0) {
        // we have a game which was extended, so take that into account and reduce elo
        $excessGoals = min(ELO_EXCESS_GOALS_MAX, $excessGoals); // limit to ELO_EXCESS_GOALS_MAX
        // linearly reduce for each excess goal, up to 1/2
        $overlyLongFactor = 1 - 0.5 * $excessGoals / ELO_EXCESS_GOALS_MAX;
    }
    else {
        $overlyLongFactor = 1;
    }

    // compute scores for this game
    $eloA = $K * ($SA - $EA) * $overlyLongFactor;
    $eloB = -$eloA; // = K (SB - EB)
    $eloAInv = $K * ($SAInv - $EA) * $overlyLongFactor; // elo, if the other team won (goals swapped)
    $eloBInv = -$eloAInv;

    return array("eloA" => $eloA, "eloB" => $eloB, "eloAInv" => $eloAInv, "eloBInv" => $eloBInv, "K" => $K);
}

// Completely rebuilds the Élő scores for all players. This is usually called every time something changes (like a game or player is added).
function updateElo() {
    $curTime = new DateTime(null, new DateTimeZone('UTC'));

    // fetch all games
    $games = getGames("0001-01-01 00:00:00", "9999-12-31 23:59:59", true); // sort in ascending date order to preserve game order (important for elo!)
    // also get a list of games before TIME_WINDOW.
    // this is only needed to properly find out how many games each player has played,
    // to decide if his bonus is over. If we use the games list above, player bonuses will be granted
    // at the beginning of each epoch, not only for his actual first games.
    $timeWindowStart = getGamesStartTime();
    $gamesOld = getGames("0001-01-01 00:00:00", $timeWindowStart->format('Y-m-d H:i:s'), true, null, false, false, true);


    // get players and reset Élő related values
    $players = array();
    foreach(getPlayers($games, $includeEloDetails=false) as $p) {
        $p["nGames"]        = 0; // number of games the player has played not longer than TIME_WINDOW ago
        $p["nGamesOld"]     = 0; // number of games the player has played before TIME_WINDOW
        $p["nGamesTotal"]   = 0; // sum of nGames and nGamesOld. This will be stored in the database to be available for computing elo previews
        $p["eloTotal"]      = ELO_INIT; // total elo points, including penalties
        $p["eloHistory"]    = array(); // how the elo total developed. It's a list of games with eloTotal after the game, play time and player names
        $p["eloLast"]       = 0; // last game's points
        $p["eloDiffBetter"] = 0; // difference to next better player
        $p["eloPenalty"]    = 0; // total penalty due to inactivity
        $p["eloPenalties"]  = array(); // list with all penalties
        $p["newcomerBonusGamesRemain"] = 0; // how many bonus games the player has remaining (will be computed here)
        $p["lastGame"]      = new DateTime("0001-01-01 00:00:00"); // date of the player's last game

        $players[$p["id"]] = $p;
    }


    // compute number of games before TIME_WINDOW
    foreach ($gamesOld as $g) {
        // get a list of all players participating in this game, excluding double
        // entries for single games
        $playerIdsUnique = array_unique(array($g["playerA1Id"], $g["playerA2Id"], $g["playerB1Id"], $g["playerB2Id"]));
        // update how many games the players have played
        foreach ($playerIdsUnique as $playerId) {
            $players[$playerId]["nGamesOld"] += 1;
        }
    }

    // compute Élő numbers
    // go through each played game and update the score of involved players
    foreach ($games as &$g) {
        // get player ids
        $a1Id = $g["playerA1Id"];
        $a2Id = $g["playerA2Id"];
        $b1Id = $g["playerB1Id"];
        $b2Id = $g["playerB2Id"];

        // when this game took place (all dates in UTC)
        $gameTime = new DateTime($g["playTime"], new DateTimeZone('UTC'));
        // get a list of all players participating in this game, excluding double
        // entries for single games
        $playerIdsUnique = array_unique(array($a1Id, $a2Id, $b1Id, $b2Id));

        // penalize inactive players before applying scores of this game
        // only once per game (for single games)
        foreach ($playerIdsUnique as $playerId) {
            $p = &$players[$playerId];
            // only penalize if the player has played at least one game
            if ($p["nGames"] > 0 && $p["playerType"] === PlayerType::Regular) {
                list($penaltyDec, $penaltyPreDec, $penaltyType) = getEloPenalty($p["lastGame"], $gameTime, $curTime);
                // store and apply the penalty now (not at the end to consider breaks when they were present)
                if ($penaltyType === EloPenaltyType::Active || $penaltyType === EloPenaltyType::Decaying) {
                    $p["eloPenalty"] += $penaltyDec;
                    $p["eloTotal"]   -= $penaltyDec;
                }

                // store all not too short penalties in the list
                if ($penaltyType != EloPenaltyType::TooShort) {
                    $p["eloPenalties"][] = array(
                        "from"        => $p["lastGame"]->format("Y-m-d H:i:s"),
                        "to"          => $gameTime->format("Y-m-d H:i:s"),
                        "value"       => $penaltyDec,
                        "valuePreDec" => $penaltyPreDec,
                        "type"        => $penaltyType,
                    );
                }
            }

            // store this as most recent game of the involved players
            $p["lastGame"] = $gameTime;
        }
        // clear the pointer, or subsequent loops will overwrite stuff (php madness...)
        unset($p);

        // update how many games the players have played
        foreach ($playerIdsUnique as $playerId) {
            $players[$playerId]["nGames"] += 1;
            $players[$playerId]["nGamesTotal"] = $players[$playerId]["nGames"] + $players[$playerId]["nGamesOld"];
        }

        // compute the score of this game
        $gameElo = computeGameElo(
            [
                "A1" => $players[$g["playerA1Id"]],
                "A2" => $players[$g["playerA2Id"]],
                "B1" => $players[$g["playerB1Id"]],
                "B2" => $players[$g["playerB2Id"]]
            ],
            $g["scoreA"], $g["scoreB"]
        );

        // score of the last game
        // if it's a dummy, don't change the elo score
        if ($players[$a1Id]["playerType"] != PlayerType::Dummy) $players[$a1Id]["eloLast"] = $gameElo["eloA"];
        if ($players[$a2Id]["playerType"] != PlayerType::Dummy) $players[$a2Id]["eloLast"] = $gameElo["eloA"];
        if ($players[$b1Id]["playerType"] != PlayerType::Dummy) $players[$b1Id]["eloLast"] = $gameElo["eloB"];
        if ($players[$b2Id]["playerType"] != PlayerType::Dummy) $players[$b2Id]["eloLast"] = $gameElo["eloB"];

        // compute total score. Make sure to add twice if a single player took on two opponents but not in singles games
        if (($a1Id === $a2Id) && ($b1Id === $b2Id)) {
            // singles game: no elo is added twice
            $a1Old = $players[$a1Id]["eloTotal"];
            $a2Old = $players[$a2Id]["eloTotal"];
            $b1Old = $players[$b1Id]["eloTotal"];
            $b2Old = $players[$b2Id]["eloTotal"];
            $players[$a1Id]["eloTotal"] = $a1Old + $players[$a1Id]["eloLast"];
            $players[$a2Id]["eloTotal"] = $a2Old + $players[$a2Id]["eloLast"];
            $players[$b1Id]["eloTotal"] = $b1Old + $players[$b1Id]["eloLast"];
            $players[$b2Id]["eloTotal"] = $b2Old + $players[$b2Id]["eloLast"];
        }
        else {
            // doubles or 1 v. 2 game
            $players[$a1Id]["eloTotal"] = $players[$a1Id]["eloTotal"] + $players[$a1Id]["eloLast"];
            $players[$a2Id]["eloTotal"] = $players[$a2Id]["eloTotal"] + $players[$a2Id]["eloLast"];
            $players[$b1Id]["eloTotal"] = $players[$b1Id]["eloTotal"] + $players[$b1Id]["eloLast"];
            $players[$b2Id]["eloTotal"] = $players[$b2Id]["eloTotal"] + $players[$b2Id]["eloLast"];
        }

        // store the points for this game
        $g["elo"]    = $gameElo["eloA"]; // use team A's value, B is always the same, just with opposite sign
        $g["eloInv"] = $gameElo["eloAInv"];
        $g["eloK"]   = $gameElo["K"];

        // store the new score in the players' history
        foreach ($playerIdsUnique as $playerId) {
            $p = &$players[$playerId];
            $p["eloHistory"][] = array(
                "gameId"   => $g["id"],
                "eloTotal" => $p["eloTotal"],
            );
        }
        unset($p);
    }
    unset($g);

    // post-process players
    foreach ($players as &$p) {
        // store how many bonus games the player still has
        if ($p["newcomerBonusGranted"] === 1) {
            $p["newcomerBonusGamesRemain"] = max(0, ELO_NEWCOMER_GAMES - $p["nGamesTotal"]);
        }
        else {
            $p["newcomerBonusGamesRemain"] = 0;
        }

        // penalize players which did not play a while since the last game
        // similar to above, but the "current game" is simply the time now
        if ($p["nGames"] > 0 && $p["playerType"] === PlayerType::Regular) {
            list($penaltyDec, $penaltyPreDec, $penaltyType) = getEloPenalty($p["lastGame"], $curTime, $curTime);

            // no decaying is possible here, since we compute until now
            if ($penaltyType === EloPenaltyType::Active) {
                $p["eloPenalty"] += $penaltyDec;
                $p["eloTotal"]   -= $penaltyDec;
            }

            if ($penaltyType != EloPenaltyType::TooShort) {
                $p["eloPenalties"][] = array(
                    "from"        => $p["lastGame"]->format("Y-m-d H:i:s"),
                    "to"          => $curTime->format("Y-m-d H:i:s"),
                    "value"       => $penaltyDec,
                    "valuePreDec" => $penaltyPreDec,
                    "type"        => $penaltyType,
                );
            }
        }
    }
    unset($p);

    // compute difference to next better player
    // start with assuming this is the best player and try to find a better one
    foreach ($players as &$p) {
        // skip players without games
        if ($p["nGames"] === 0)
            continue;

        $betterP = null;
        foreach ($players as $p2) {
            // search for better players
            if ($p2["isActive"] === true && $p2["nGames"] > 0 && $p2["eloTotal"] > $p["eloTotal"]) {
                if ($betterP === null) {
                    // initialize with taking any better player
                    $betterP = $p2;
                }
                else {
                    // only accept players in between
                    if ($p2["eloTotal"] < $betterP["eloTotal"])
                        $betterP = $p2;
                }
            }
        }
        // if there was no better player, leave the difference as 0, otherwise update it
        if ($betterP !== null)
            $p["eloDiffBetter"] = $betterP["eloTotal"] - $p["eloTotal"];
    }
    unset($p);

    // write all the computed stuff to the database
    try{
        global $pdo;
        foreach ($players as $p) {
            $stmt = $pdo->prepare(
                "UPDATE " . PLAYERS_TABLE .
                " SET nGamesTotal = :nGamesTotal, eloTotal = :eloTotal, eloHistory = :eloHistory, eloLast = :eloLast, eloDiffBetter = :eloDiffBetter," .
                " eloPenalty = :eloPenalty, eloPenalties = :eloPenalties, newcomerBonusGamesRemain = :newcomerBonusGamesRemain" .
                " WHERE " . PLAYERS_TABLE . ".id = :id"
            );
            $stmt->bindValue(":nGamesTotal",              $p["nGamesTotal"],               PDO::PARAM_STR);
            $stmt->bindValue(":eloTotal",                 $p["eloTotal"],                  PDO::PARAM_STR);
            $stmt->bindValue(":eloHistory",               json_encode($p["eloHistory"]),   PDO::PARAM_STR);
            $stmt->bindValue(":eloLast",                  $p["eloLast"],                   PDO::PARAM_STR);
            $stmt->bindValue(":eloDiffBetter",            $p["eloDiffBetter"],             PDO::PARAM_STR);
            $stmt->bindValue(":eloPenalty",               $p["eloPenalty"],                PDO::PARAM_STR);
            $stmt->bindValue(":eloPenalties",             json_encode($p["eloPenalties"]), PDO::PARAM_STR);
            $stmt->bindValue(":newcomerBonusGamesRemain", $p["newcomerBonusGamesRemain"],  PDO::PARAM_STR);
            $stmt->bindValue(":id",                       $p["id"],                        PDO::PARAM_STR);
            $stmt->execute();
        }
        foreach ($games as $g) {
            $stmt = $pdo->prepare("UPDATE " . GAMES_TABLE . " SET elo = :elo, eloInv = :eloInv, eloK = :eloK WHERE " . GAMES_TABLE . ".id = :id");
            $stmt->bindValue(":elo",    $g["elo"],    PDO::PARAM_STR);
            $stmt->bindValue(":eloInv", $g["eloInv"], PDO::PARAM_STR);
            $stmt->bindValue(":eloK",   $g["eloK"],   PDO::PARAM_STR);
            $stmt->bindValue(":id",     $g["id"],     PDO::PARAM_STR);
            $stmt->execute();
        }
    }
    catch (PDOException $e) {
        return null;
    }
    return true;
}



// Resets Élő-related stats in all players. Those are rebuilt whenever updateElo() is called.
function resetPlayerEloValues() {
    foreach(getPlayers(null, $includeEloDetails=false) as $p) {
        $p["nGames"]        = 0;
        $p["nGamesOld"]     = 0;
        $p["nGamesTotal"]   = 0;
        $p["eloTotal"]      = ELO_INIT;
        $p["eloHistory"]    = array();
        $p["eloLast"]       = 0;
        $p["eloDiffBetter"] = 0;
        $p["eloPenalty"]    = 0;
        $p["eloPenalties"]  = array();
        $p["newcomerBonusGamesRemain"] = 0;
        $p["lastGame"]      = new DateTime("0001-01-01 00:00:00");

        // write to the database
        try{
            global $pdo;
            $stmt = $pdo->prepare(
                "UPDATE " . PLAYERS_TABLE .
                " SET nGamesTotal = :nGamesTotal, eloTotal = :eloTotal, eloHistory = :eloHistory, eloLast = :eloLast, eloDiffBetter = :eloDiffBetter," .
                " eloPenalty = :eloPenalty, eloPenalties = :eloPenalties, newcomerBonusGamesRemain = :newcomerBonusGamesRemain" .
                " WHERE " . PLAYERS_TABLE . ".id = :id"
            );
            $stmt->bindValue(":nGamesTotal",              $p["nGamesTotal"],               PDO::PARAM_STR);
            $stmt->bindValue(":eloTotal",                 $p["eloTotal"],                  PDO::PARAM_STR);
            $stmt->bindValue(":eloHistory",               json_encode($p["eloHistory"]),   PDO::PARAM_STR);
            $stmt->bindValue(":eloLast",                  $p["eloLast"],                   PDO::PARAM_STR);
            $stmt->bindValue(":eloDiffBetter",            $p["eloDiffBetter"],             PDO::PARAM_STR);
            $stmt->bindValue(":eloPenalty",               $p["eloPenalty"],                PDO::PARAM_STR);
            $stmt->bindValue(":eloPenalties",             json_encode($p["eloPenalties"]), PDO::PARAM_STR);
            $stmt->bindValue(":newcomerBonusGamesRemain", $p["newcomerBonusGamesRemain"],  PDO::PARAM_STR);
            $stmt->bindValue(":id",                       $p["id"],                        PDO::PARAM_STR);
            $stmt->execute();
        } catch (PDOException $e) {
            return null;
        }
    }

    return true;
}



// Adds a game to the database.
// player{A,B}{1,2}: names of the players in the two teams
// score{A,B}: how many goals each team scored
// playTime: when the game took place
// comment: an optional string describing the game
function addGame($playerA1, $playerA2, $playerB1, $playerB2, $scoreA, $scoreB, $playTime, $comment="") {
    // find player ids
    // although this leads to 4 database queries, it's fine since adding games is not frequent and not performance critical
    $idA1 = findPlayerId(cleanInput($playerA1));
    $idA2 = findPlayerId(cleanInput($playerA2));
    $idB1 = findPlayerId(cleanInput($playerB1));
    $idB2 = findPlayerId(cleanInput($playerB2));

    // store the game
    global $pdo;
    try {
        $stmt = $pdo->prepare("INSERT INTO " . GAMES_TABLE . "
                   (ignored,  playerA1,  playerA2,  scoreA,  playerB1,  playerB2,  scoreB,  playTime,  comment)
            VALUES (0,           :idA1,     :idA2, :scoreA,     :idB1,     :idB2, :scoreB, :playTime, :comment)"
        );
        $stmt->bindValue(":idA1",     $idA1,                       PDO::PARAM_STR);
        $stmt->bindValue(":idA2",     $idA2,                       PDO::PARAM_STR);
        $stmt->bindValue(":idB1",     $idB1,                       PDO::PARAM_STR);
        $stmt->bindValue(":idB2",     $idB2,                       PDO::PARAM_STR);
        $stmt->bindValue(":scoreA",   cleanInput($scoreA),         PDO::PARAM_STR);
        $stmt->bindValue(":scoreB",   cleanInput($scoreB),         PDO::PARAM_STR);
        $stmt->bindValue(":playTime", cleanInput($playTime, true), PDO::PARAM_STR);
        $stmt->bindValue(":comment",  cleanInput($comment),        PDO::PARAM_STR);
        $stmt->execute();
    }
    catch (PDOException $e) {
        return false;
    }
    // get the ID of this game
    $gid = $pdo->lastInsertId();

    updateElo();

    // load updated stats and send mails
    if (SEND_MAILS) {
        $playersTable = PLAYERS_TABLE;
$sqlQuery = <<<SQL
            SELECT name, email, eloTotal, eloLast
            FROM $playersTable
            WHERE
                id = :idA1 OR
                id = :idA2 OR
                id = :idB1 OR
                id = :idB2;
SQL;
        $stmt = $pdo->prepare($sqlQuery);
        $stmt->bindValue(":idA1", $idA1, PDO::PARAM_STR);
        $stmt->bindValue(":idA2", $idA2, PDO::PARAM_STR);
        $stmt->bindValue(":idB1", $idB1, PDO::PARAM_STR);
        $stmt->bindValue(":idB2", $idB2, PDO::PARAM_STR);
        // convert result to an array
        $newStats = array();
        try {
            $stmt->execute();
            while ($row = $stmt->fetch(PDO::FETCH_NUM, PDO::FETCH_ORI_NEXT)) {
                $newStats[] = array(
                    "name"     => $row[0],
                    "email"    => $row[1],
                    "eloTotal" => floatval($row[2]),
                    "eloLast"  => floatval($row[3]),
                );
            }
            $stmt = null;
        }
        catch (PDOException $e) {
            return null;
        }

        if (strlen($comment) > 0)
            $commentLine = $comment . "\n";
        else
            $commentLine = "";

        foreach ($newStats as $ns) {
            if (strlen($ns["email"]) > 0) {
                if ($ns["eloLast"] > 0)
                    $wonLost = "won";
                else
                    $wonLost = "lost";
                $mailText = "Hi " . ucfirst($ns["name"]) . ",\n" .
                        "A game has been entered for you:\n" .
                        "(" . ucfirst($playerA1) . "," . ucfirst($playerA2) . ") " . $scoreA . ":" . $scoreB . " (" . ucfirst($playerB1) . "," . ucfirst($playerB2) . ")\n" .
                        $commentLine .
                        "You " . $wonLost . " " . round(abs($ns["eloLast"]), 2) . " elo points and now have " . round($ns["eloTotal"], 2) . " in total.\n" .
                        "If you think this is an error, please contact the kicker admins and provide them the game id " . $gid . ".";
                sendMail($ns["email"], MAIL_HEADER, $mailText);
            }
        }
    }
    return true;
}

// Sets the active state of the game with the given ID.
function setGameActive($gameId, $active) {
    global $pdo;
    if ($active) $ignored = "0"; else $ignored = "1";
    try{
        $stmt = $pdo->prepare("UPDATE " . GAMES_TABLE . " SET ignored = :ignored WHERE id = :id");
        $stmt->bindValue(":id",      $gameId,  PDO::PARAM_STR);
        $stmt->bindValue(":ignored", $ignored, PDO::PARAM_STR);
        $stmt->execute();
        // if there is no game with this ID, return false
        if($stmt->rowCount() === 0)
            return false;
    }
    catch (PDOException $e) {
        return false;
    }
    return true;
}

// Returns a list of all played games.
// time{From,To}: restrict games to this timespan
// sortAsc: sort with by date, ascending instead of descending
// playerName: only return games of this player
// asDict: selects whether to return a list of games sorted by game time, or as a dict with index game id. Default is a list (false).
// includeIgnoredGames: also include ignored games
// includeOldGames: also include games which are older than TIME_WINDOW
function getGames(
    $timeFrom            = null,
    $timeTo              = null,
    $sortAsc             = false,
    $playerName          = null,
    $asDict              = false,
    $includeIgnoredGames = false,
    $includeOldGames     = false
) {
    if ($timeFrom === null) $timeFrom="0001-01-01 00:00:00";
    // see if we need to adjust timeFrom according to TIME_WINDOW
    if ($includeOldGames == false) { // only if includeOldGames is not set, otherwise ignore the TIME_WINDOW
        $timeFromObj = new DateTime($timeFrom, new DateTimeZone('UTC'));
        $curTimeObj  = new DateTime('now',     new DateTimeZone('UTC'));
        $timeWindowStartObj = getGamesStartTime();
        if ($timeFromObj < $timeWindowStartObj) {
            $timeFrom = $timeWindowStartObj->format('Y-m-d H:i:s');
        }
    }

    if ($timeTo === null)   $timeTo="9999-12-31 23:59:59";
    if ($sortAsc) $sortStr="ASC"; else $sortStr = "DESC";

    // mysql and sqlite have different syntax for comparing dates :/
    if (DB_TYPE === "mysql") {
        $filtStr = "G.playTime >= CONVERT(:timeFrom, DateTime) AND G.playTime <= CONVERT(:timeTo, DateTime)";
    }
    elseif (DB_TYPE === "sqlite") {
        $filtStr = "G.playTime >= datetime(:timeFrom) AND G.playTime <= datetime(:timeTo)";
    }
    if ($playerName !== null)           $filtStr .= "AND (P1.name = :pName1 OR P2.name = :pName2 OR P3.name = :pName3 OR P4.name = :pName4) ";
    if ($includeIgnoredGames === false) $filtStr .= "AND NOT G.ignored ";

    // copy constants into variable for easier substitution
    $playersTable = PLAYERS_TABLE;
    $gamesTable   = GAMES_TABLE;
$sqlQuery = <<<SQL
        SELECT
            G.id,
            G.ignored,
            G.playTime,
            G.playerA1,
            G.playerA2,
            G.playerB1,
            G.playerB2,
            P1.name AS a1Name,
            P2.name AS a2Name,
            P3.name AS b1Name,
            P4.name AS b2Name,
            G.scoreA,
            G.scoreB,
            G.elo,
            G.eloInv,
            G.eloK,
            G.comment
        FROM $gamesTable AS G
        INNER JOIN $playersTable AS P1
          ON G.playerA1 = P1.id
        INNER JOIN $playersTable AS P2
          ON G.playerA2 = P2.id
        INNER JOIN $playersTable AS P3
          ON G.playerB1 = P3.id
        INNER JOIN $playersTable AS P4
          ON G.playerB2 = P4.id
        WHERE
            $filtStr
        ORDER BY
            G.playTime $sortStr;
SQL;
    global $pdo;
    $stmt = $pdo->prepare($sqlQuery);
    $stmt->bindValue(":timeFrom",   $timeFrom,   PDO::PARAM_STR);
    $stmt->bindValue(":timeTo",     $timeTo,     PDO::PARAM_STR);
    if ($playerName !== null) {
        $stmt->bindValue(":pName1", $playerName, PDO::PARAM_STR);
        $stmt->bindValue(":pName2", $playerName, PDO::PARAM_STR);
        $stmt->bindValue(":pName3", $playerName, PDO::PARAM_STR);
        $stmt->bindValue(":pName4", $playerName, PDO::PARAM_STR);
    }

    // convert result to an array
    $ret = array();
    try {
        $stmt->execute();
        while ($row = $stmt->fetch(PDO::FETCH_NUM, PDO::FETCH_ORI_NEXT)) {
            // if we should return this as a list, always append new games, otherwise use the game id as key
            if ($asDict === false)
                $pos = sizeof($ret);
            else
                $pos = $row[0];

            $ret[$pos] = array(
                "id"         => intval($row[0]),
                "ignored"    => intval($row[1]),
                "playTime"   => $row[2],
                "playerA1Id" => intval($row[3]),
                "playerA2Id" => intval($row[4]),
                "playerB1Id" => intval($row[5]),
                "playerB2Id" => intval($row[6]),
                "playerA1"   => $row[7],
                "playerA2"   => $row[8],
                "playerB1"   => $row[9],
                "playerB2"   => $row[10],
                "scoreA"     => intval($row[11]),
                "scoreB"     => intval($row[12]),
                "elo"        => floatval($row[13]),
                "eloInv"     => floatval($row[14]),
                "eloK"       => floatval($row[15]),
                "comment"    => $row[16],
            );
        }
        $stmt = null;
    }
    catch (PDOException $e) {
        return null;
    }
    return $ret;
}

// Filters a list of games and only returns those which were 1 on 1 games.
function filterForSingleOrDoubleGames($games, $getDoubles=true) {
    $ret = array();
    foreach ($games as $g) {
        $a1 = $g["playerA1Id"];
        $a2 = $g["playerA2Id"];
        $b1 = $g["playerB1Id"];
        $b2 = $g["playerB2Id"];
        if (($a1 === $a2) && ($b1 === $b2)) {
            // single game
            if (!$getDoubles)
                $ret[] = $g;
        } else {
            // double game
            if ($getDoubles)
                $ret[] = $g;
        }
    }
    return $ret;
}



// Adds a new player to the database.
// name: The players name. Only a-z, 0-9, - and _
// isGuest: set to "1" to add a guest (regular guest whith no penalty but elo; not the special "guest" without even elo). "Z_" will be prepended if not present.
// email: e-mail address of the player (can be "")
// newcomerBonus: if the player should have a newcomer bonus, give his personal percentage of the regular Élő K value here (may be >100 as well). Default is 100..
function addPlayer($name, $isGuest=0, $email="", $newcomerBonus=null) {
    global $pdo;

    // check if the player already exists
    $pName = cleanName($name);
    if (strlen($pName) === 0) return false;
    if ($isGuest && !startsWith($pName, "z_")) $pName = "z_" . $pName;
    $stmt = $pdo->prepare("SELECT id FROM " . PLAYERS_TABLE . " WHERE name = :pName");
    $stmt->bindValue(":pName", $pName, PDO::PARAM_STR);
    try {
        $stmt->execute();
        $ret = $stmt->fetch(PDO::FETCH_NUM, PDO::FETCH_ORI_NEXT);
        $stmt = null;
        if ($ret && count($ret) > 0)
            $pId = intval($ret[0]);
        else
            $pId = null;
    }
    catch (PDOException $e) {
        return false;
    }
    if ($pId !== null)
        return false; // player exists


    if ($isGuest) $playerType = 1; else $playerType = 0;
    // guests are not allowed to be newcomers
    if ($isGuest && $newcomerBonus != null)
        return false;
    if ($newcomerBonus != null) {
        $newcomerBonusGranted = 1;
        $newcomerBonus = floatval($newcomerBonus);
    } else {
        $newcomerBonusGranted = 0;
        $newcomerBonus = 100;
    }

    try {
        $stmt = $pdo->prepare("INSERT INTO " . PLAYERS_TABLE . "
                   ( name,  playerType,  newcomerBonusGranted,  newcomerBonus,  email)
            VALUES (:name, :playerType, :newcomerBonusGranted, :newcomerBonus, :email)"
        );
        $stmt->bindValue(":name",                 $pName,                PDO::PARAM_STR);
        $stmt->bindValue(":playerType",           $playerType,           PDO::PARAM_STR);
        $stmt->bindValue(":newcomerBonusGranted", $newcomerBonusGranted, PDO::PARAM_STR);
        $stmt->bindValue(":newcomerBonus",        $newcomerBonus,        PDO::PARAM_STR);
        $stmt->bindValue(":email",                $email,                PDO::PARAM_STR);
        $stmt->execute();
    }
    catch (PDOException $e) {
        var_dump($email);
        print($e);
        return false;
    }

    updateElo();
    return true;
}



// Returns a dict of all players with additional computed attributes like total played games, score, etc.
// The dict key is the player's name.
// games: If given, player stats are computed, e.g. played games, if active, efficiency, ...
// includeEloDetails: whether to fill eloHistory and eloPenalties.
function getPlayers($games=null, $includeEloDetails=false) {
    global $pdo;

    $playersTable = PLAYERS_TABLE; // copy constant into variable for easier substitution
    if ($includeEloDetails) {
        $queryHistStr = "eloHistory";
        $queryPensStr = "eloPenalties";
        $allGames = getGames("0001-01-01 00:00:00", "9999-12-31 23:59:59", false, null, true); // get all games as dict for elo details
    }
    else {
        // simply return 0
        $queryHistStr = "0";
        $queryPensStr = "0";
    }

$sqlQuery = <<<SQL
        SELECT
            id, name, playerType, nGamesTotal, eloTotal, $queryHistStr, eloLast, eloDiffBetter, eloPenalty, $queryPensStr, newcomerBonusGranted, newcomerBonus, newcomerBonusGamesRemain
        FROM
            $playersTable
        ORDER BY
            name ASC
SQL;

    $stmt = $pdo->prepare($sqlQuery);
    // convert result to an array
    $players = array();
    try {
        $stmt->execute();
        while ($row = $stmt->fetch(PDO::FETCH_NUM, PDO::FETCH_ORI_NEXT)) {
            $pName = $row[1];
            $players[$pName] = array(
                "id"            => intval($row[0]),
                "name"          => $row[1],
                "playerType"    => intval($row[2]),
                "nGamesTotal"   => intval($row[3]),
                "gamesWon"      => 0,
                "gamesLost"     => 0,
                "gamesTotal"    => 0,
                "score"         => 0,
                "winRatio"      => 0,
                "efficiency"    => 0,
                "eloTotal"      => floatval($row[4]),
                "eloHistory"    => $row[5],
                "eloLast"       => floatval($row[6]),
                "eloDiffBetter" => floatval($row[7]),
                "eloPenalty"    => floatval($row[8]),
                "eloPenalties"  => $row[9],
                "newcomerBonusGranted"     => intval($row[10]),
                "newcomerBonus"            => floatval($row[11]),
                "newcomerBonusGamesRemain" => intval($row[12]),
                "totalGoalsP"   => 0,
                "totalGoalsN"   => 0,
                "lastGame"      => null,
                "isActive"      => false,
            );

            // add time and player names in elo history
            if ($includeEloDetails === true) {
                $histFromDB = json_decode($players[$pName]["eloHistory"], true);
                // go through the history and append an entry in $eloHistoryUpdated
                $eloHistoryUpdated = array();
                foreach ($histFromDB as $hEnt) {
                    $g = $allGames[$hEnt["gameId"]];
                    $eloHistoryUpdated[] = array(
                        "gameId"   => $hEnt["gameId"],
                        "eloTotal" => $hEnt["eloTotal"],
                        "eloGame"  => $g["elo"],
                        "time"     => $g["playTime"],
                        "playerA1" => $g["playerA1"],
                        "playerA2" => $g["playerA2"],
                        "playerB1" => $g["playerB1"],
                        "playerB2" => $g["playerB2"],
                        "scoreA"   => $g["scoreA"],
                        "scoreB"   => $g["scoreB"],
                    );
                }
                // save back the updated version
                $players[$pName]["eloHistory"] = $eloHistoryUpdated;
            }
        }
    }
    catch (PDOException $e) {
        return null;
    }

    if ($games !== null) {
        // compute game stats for each player
        foreach ($games as $g) {
            $a1 = $g["playerA1"];
            $a2 = $g["playerA2"];
            $b1 = $g["playerB1"];
            $b2 = $g["playerB2"];
            $s  = $g["scoreA"] - $g["scoreB"];

            // store the last game of each player
            foreach (array_unique(array($a1, $a2, $b1, $b2)) as $p) {
                if ($players[$p]["lastGame"] < $g["playTime"]) {
                    $players[$p]["lastGame"] = $g["playTime"];
                }
            }

            // won and lost games, total scores and total goals
            $players[$a1]["score"] += $s;
            $players[$a1]["totalGoalsP"] += $g["scoreA"];
            $players[$a1]["totalGoalsN"] += $g["scoreB"];
            if ($s > 0) // A won
                $players[$a1]["gamesWon"] += 1;
            else // A lost
                $players[$a1]["gamesLost"] += 1;
            if ($a2 !== $a1) {
                // also add a2 if the player was not alone
                $players[$a2]["score"] += $s;
                $players[$a2]["totalGoalsP"] += $g["scoreA"];
                $players[$a2]["totalGoalsN"] += $g["scoreB"];
                if ($s > 0)
                    $players[$a2]["gamesWon"] += 1;
                else
                    $players[$a2]["gamesLost"] += 1;
            }

            // same for team B
            $players[$b1]["score"] -= $s;
            $players[$b1]["totalGoalsP"] += $g["scoreB"];
            $players[$b1]["totalGoalsN"] += $g["scoreA"];
            if ($s < 0) // B won
                $players[$b1]["gamesWon"] += 1;
            else // B lost
                $players[$b1]["gamesLost"] += 1;
            if ($b2 !== $b1) {
                $players[$b2]["score"] -= $s;
                $players[$b2]["totalGoalsP"] += $g["scoreB"];
                $players[$b2]["totalGoalsN"] += $g["scoreA"];
                if ($s < 0)
                    $players[$b2]["gamesWon"] += 1;
                else
                    $players[$b2]["gamesLost"] += 1;
            }
        }

        // compute total plays and ratios and mark active players
        $curTime = new DateTime(null, new DateTimeZone('UTC'));
        $inactiveDeadline = $curTime->sub(new DateInterval(PLAYER_INACTIVE_TIME));
        foreach($players as &$p) {
            $gamesTotal = $p["gamesLost"] + $p["gamesWon"];
            $p["gamesTotal"] = $gamesTotal;
            if ($gamesTotal > 0) {
                $p["winRatio"]   = $p["gamesWon"] / $gamesTotal;
                $p["efficiency"] = $p["score"]    / $gamesTotal;
            }

            if ($p["lastGame"] !== null) {
                $pLastGameTime = new DateTime($p["lastGame"], new DateTimeZone('UTC'));
                if ($pLastGameTime > $inactiveDeadline)
                    $p["isActive"] = true;
            }
        }
        unset($p);
    }

    return $players;
}



// Returns all player names
function getPlayerNames() {
    $players = getPlayers();
    if ($players === null)
        dieBadReq("Could not load players!");
    $pNames = array();
    foreach ($players as $pName => $p) {
        $pNames[] = $pName;
    }
    return $pNames;
}

// Returns all dummy player names (special guest accounts)
function getDummyPlayerNames() {
    $players = getPlayers();
    if ($players === null)
        dieBadReq("Could not load players!");
    $pNames = array();
    foreach ($players as $pName => $p) {
        if ($p["playerType"] === PlayerType::Dummy)
            $pNames[] = $pName;
    }
    return $pNames;
}

// Returns names for active players (at least one game and last game not too long ago)
function getActivePlayerNames() {
    $players = getPlayers(getGames("0001-01-01 00:00:00", "9999-12-31 23:59:59"));
    if ($players === null)
        dieBadReq("Could not load players and / or games!");
    $pNames = array();
    foreach ($players as $pName => $p) {
        if ($p["isActive"] === true)
            $pNames[] = $pName;
    }
    return $pNames;
}

// Returns names for players who have played at least one game
function getPlayersWithGamesNames() {
    $players = getPlayers(getGames("0001-01-01 00:00:00", "9999-12-31 23:59:59"));
    if ($players === null)
        dieBadReq("Could not load players and / or games!");
    $pNames = array();
    foreach ($players as $pName => $p) {
        if ($p["gamesTotal"] > 0)
            $pNames[] = $pName;
    }
    return $pNames;
}

// Takes a list of players and returns only those which have played at least one game since the given time.
function filterPlayersByLastGame($players, $timeFrom="0001-01-01 00:00:00") {
    $ret = array();

    // convert timeFrom
    $timeFrom = new DateTime($timeFrom, new DateTimeZone('UTC'));
    foreach ($players as $p) {
        if ($p["lastGame"] != null){
            $lastGameTime = new DateTime($p["lastGame"], new DateTimeZone('UTC'));
            if ($lastGameTime > $timeFrom)
                $ret[] = $p;
        }
    }

    return $ret;
}

// Returns a player's id, selected by it's name.
function findPlayerId($pName) {
    global $pdo;
    $stmt = $pdo->prepare("SELECT id FROM " . PLAYERS_TABLE . " WHERE name = :pName");
    $stmt->bindValue(":pName", $pName, PDO::PARAM_STR);
    try {
        $stmt->execute();
        $pId = $stmt->fetch(PDO::FETCH_NUM, PDO::FETCH_ORI_NEXT)[0];
        $pId = intval($pId);
        $stmt = null;
    }
    catch (PDOException $e) {
        return null;
    }
    return $pId;
}
// Returns a player's name, selected by it's id.
function findPlayerName($pId) {
    global $pdo;
    $stmt = $pdo->prepare("SELECT name FROM " . PLAYERS_TABLE . " WHERE id = :pId");
    $stmt->bindValue(":pId", $pId, PDO::PARAM_STR);
    try {
        $stmt->execute();
        $pName = $stmt->fetch(PDO::FETCH_NUM, PDO::FETCH_ORI_NEXT)[0];
        $pId = intval($pId);
        $stmt = null;
    }
    catch (PDOException $e) {
        return null;
    }
    return $pName;
}



// Returns a ranking of all players, sorted by eloTotal, score or efficiency.
// $sortKey needs to be one of the keys found in a "players" entry (like eloTotal, score, efficiency, gamesLost, etc.).
function getRanking($sortKey, $players) {
    // put players in a regular array (not a dict) and add their names
    $ranking = array();
    foreach ($players as $pName => $p) {
        $p["name"] = $pName;
        $p["rank"] = 0;
        $ranking[] = $p;
    }

    // sort the list by the selected sort key
    // function which compares two players and returns 0 if they are equal,
    // -1 if a > b and +1 if a < b.
    // function cmpPlayers($a, $b) {
    $cmpPlayers = function($a, $b) use ($sortKey) {
        if ($a[$sortKey] == $b[$sortKey]) {
            return 0;
        }
        return ($a[$sortKey] > $b[$sortKey]) ? -1 : 1;
    };
    usort($ranking, $cmpPlayers);

    // store player rank (1 is best)
    for ($i = 0; $i < count($ranking); $i++)
        $ranking[$i]["rank"] = $i+1;

    return $ranking;
}
