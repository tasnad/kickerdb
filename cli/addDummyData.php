<?php
define("WEBSITE_PHP_DEF", true);
require_once(__DIR__ . "/../common.php");
checkIsCLI();
require_once(__DIR__ . "/../db.php");
require_once(__DIR__ . "/../apiFunctions.php");

function addDummyData() {
    $pdo = $GLOBALS['pdo'];

    $pdo->exec("INSERT INTO `" . PLAYERS_TABLE . "`
        (`id`, `name`,   `playerType`, `nGamesTotal`, `eloTotal`, `eloHistory`, `eloLast`, `eloDiffBetter`, `eloPenalty`, `eloPenalties`, `newcomerBonusGranted`, `newcomerBonus`, `newcomerBonusGamesRemain`, `email`             ) VALUES
        (1,    'dummy1', 0,            0,             1000,       '',           0,         0,               0,            '',             0,                      100,             0,                          'dummy1@example.com'),
        (2,    'dummy2', 0,            0,             1000,       '',           0,         0,               0,            '',             1,                      70,              10,                         'dummy2@example.com'),
        (3,    'dummy3', 0,            0,             1000,       '',           0,         0,               0,            '',             0,                      100,             0,                          'dummy3@example.com'),
        (4,    'dummy4', 0,            0,             1000,       '',           0,         0,               0,            '',             0,                      100,             0,                          'dummy4@example.com'),
        (5,    'dummy5', 1,            0,             1000,       '',           0,         0,               0,            '',             0,                      100,             0,                          'dummy5@example.com')
    ");

    $pdo->exec("INSERT INTO `" . GAMES_TABLE . "`
        (`id`, `ignored`, `playTime`,            `playerA1`, `playerA2`, `playerB1`, `playerB2`, `scoreA`, `scoreB`, `elo`, `eloInv`, `eloK`, `comment`) VALUES
        (1,    0,         '2000-01-01 12:00:00', 1,          2,          3,          4,          10,       7,        0,     0,        0,      'test game 1'),
        (2,    1,         '2021-01-02 11:00:00', 1,          2,          3,          4,          2,        10,       0,     0,        0,      'test game 2'),
        (3,    0,         '2021-01-02 12:00:00', 1,          2,          3,          4,          2,        10,       0,     0,        0,      'test'),
        (4,    0,         '2021-01-03 12:00:00', 1,          1,          4,          4,          17,       19,       0,     0,        0,      'we won! 😃'),
        (5,    0,         '2021-01-04 12:00:00', 1,          1,          4,          4,          17,       19,       0,     0,        0,      'we won again! 😃')
    ");
}

addDummyData();
updateElo();