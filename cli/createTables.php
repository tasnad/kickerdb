<?php
define("WEBSITE_PHP_DEF", true);
require_once(__DIR__ .  "/../common.php");
checkIsCLI();
require_once(__DIR__ .  "/../db.php");

/**
 * create tables
 */
function createTables() {
    $pdo = $GLOBALS['pdo'];

    $commands = [
        "CREATE TABLE IF NOT EXISTS `" . PLAYERS_TABLE . "` (
            `id` int(11) NOT NULL,
            `name` varchar(70) COLLATE utf8mb4_unicode_ci NOT NULL,
            `playerType` tinyint(1) NOT NULL,
            `nGamesTotal` int(11) DEFAULT NULL,
            `eloTotal` float DEFAULT NULL,
            `eloHistory` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
            `eloLast` float DEFAULT NULL,
            `eloDiffBetter` float DEFAULT NULL,
            `eloPenalty` float DEFAULT NULL,
            `eloPenalties` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
            `newcomerBonusGranted` tinyint(1) NOT NULL,
            `newcomerBonus` float DEFAULT NULL,
            `newcomerBonusGamesRemain` int(11) DEFAULT NULL,
            `email` tinytext COLLATE utf8mb4_unicode_ci NOT NULL
        )",
        "ALTER TABLE `" . PLAYERS_TABLE . "` ADD PRIMARY KEY (`id`);",
        "ALTER TABLE `" . PLAYERS_TABLE . "` MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;",
        //
        "CREATE TABLE IF NOT EXISTS `" . GAMES_TABLE . "` (
            `id` int(11) NOT NULL,
            `ignored` tinyint(1) NOT NULL,
            `playTime` datetime NOT NULL,
            `playerA1` int(11) NOT NULL,
            `playerA2` int(11) NOT NULL,
            `playerB1` int(11) NOT NULL,
            `playerB2` int(11) NOT NULL,
            `scoreA` int(11) NOT NULL,
            `scoreB` int(11) NOT NULL,
            `elo` float DEFAULT NULL,
            `eloInv` float DEFAULT NULL,
            `eloK` float DEFAULT NULL,
            `comment` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
        )",
        "ALTER TABLE `" . GAMES_TABLE . "` ADD PRIMARY KEY (`id`);",
        "ALTER TABLE `" . GAMES_TABLE . "` MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;"
    ];

    foreach ($commands as $command) {
        $pdo->exec($command);
    }
}

createTables();
