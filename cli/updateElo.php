<?php
define("WEBSITE_PHP_DEF", true);
require_once(__DIR__ . "/../common.php");
checkIsCLI();
require_once(__DIR__ . "/../apiFunctions.php");

if (updateElo())
    print(json_encode("Elo points updated."));
else
    dieBadReq("Error updating the Elo points!");
