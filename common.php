<?php
if(!defined("WEBSITE_PHP_DEF")) {
    header("HTTP/1.1 401 Unauthorized");
    die("Access denied");
}
require_once("config.php");

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
if (defined("SMTP_HOST")) {
    require 'vendor/autoload.php';
}


// Assures that the file which calls this function is included from another php file.
function checkGuard($json=false) {
    if(!defined("WEBSITE_PHP_DEF")) {
        header("HTTP/1.1 401 Unauthorized");
        if ($json)
            die(json_encode("Access denied"));
        else
            die("Access denied");
    }
}

// Checks if access is granted by: API secret in URL or cookie or call from CLI.
// Stores the secret in a cookie.
// Also stores the admin token in a cookie if present.
function checkAccess($json=false) {
    if( (isset($_GET["secret"]) && $_GET["secret"] === KICKER_SECRET)
        ||
        (isset($_COOKIE["secret"]) && $_COOKIE["secret"] === KICKER_SECRET)
        ||
        (php_sapi_name() === "cli")
    ){
        // access granted
    }
    else {
        header("HTTP/1.1 401 Unauthorized");
        if ($json)
            die(json_encode("Access denied"));
        else
            die("Access denied");
    }
}

function checkIsCLI() {
    if( php_sapi_name() !== "cli"){
        header("HTTP/1.1 401 Unauthorized");
        if ($json)
            die(json_encode("Access denied"));
        else
            die("Access denied");
    }
}

// Checks if the admin token was passed as URL or cookie.
function checkAdminAccess($json=false) {
    if( (isset($_GET["admin_token"]) && $_GET["admin_token"] === KICKER_ADMIN_TOKEN)
        ||
        (isset($_COOKIE["admin_token"]) && $_COOKIE["admin_token"] === KICKER_ADMIN_TOKEN)
    ){
        // access granted
    }
    else {
        header("HTTP/1.1 401 Unauthorized");
        if ($json)
            die(json_encode("Access denied"));
        else
            die("Access denied");
    }
}

// Stores URL parameters as cookies.
function setCookiesFromUrl() {
    global $cookiesSent;
    if (!$cookiesSent) {
        if(isset($_GET["secret"]))
            setcookieCompat("secret", $_GET["secret"]);
        if(isset($_GET["admin_token"]))
            setcookieCompat("admin_token", $_GET["admin_token"]);
        if(isset($_GET["storePlayer"]))
            setcookieCompat("player", $_GET["storePlayer"]);
        $cookiesSent = true;
    }
}



// Checks if the haystack starts with needle.
function startsWith($haystack, $needle)
{
    $needleLen = strlen($needle);
    return (substr($haystack, 0, $needleLen) === $needle);
}



// Cleans user input from html tags etc.
function cleanInput($inp, $dateTime=false) {
    // Strip HTML Tags
    $ret = strip_tags($inp);
    // Clean up things like &amp;
    $ret = html_entity_decode($ret);
    // Strip out any url-encoded stuff
    $ret = urldecode($ret);
    // // Replace non-AlNum characters with space
    // if ($dateTime)
    //     $ret = preg_replace("/[^A-Za-z0-9:-]/", " ", $ret);
    // else
    //     $ret = preg_replace("/[^A-Za-z0-9]/", " ", $ret);
    // // Replace Multiple spaces with single space
    // $ret = preg_replace("/ +/", " ", $ret);
    // Trim the string of leading/trailing space
    $ret = trim($ret);
    return $ret;
}

// Cleans user input from html tags etc. which is supposed to be a player name.
// Will also convert to lowercase.
function cleanName($inp) {
    $ret = preg_replace("/[^a-zA-Z0-9_-]+/", "", $inp);;
    return strtolower($ret);
}



// Prints message and quits.
function dieBadReq($msg) {
    header("HTTP/1.1 400 Bad Request");
    die(json_encode($msg));
}



function setcookieCompat($name, $value) {
    if (PHP_VERSION_ID < 70300) {
        // hack for php < 7.3
        setcookie($name, $value, 0, SUB_PATH . "; samesite=strict", SITE_TLD, true, false);
    } else {
        setcookie($name, $value, [
            'expires' => 0,
            'path' => SUB_PATH,
            'domain' => SITE_TLD,
            'secure' => true,
            'httponly' => false,
            'samesite' => 'Strict',
        ]);
    }
}



// Includes a CSS file if a cookie with an available theme name was set.
function loadTheme() {
    if (isset($_COOKIE["theme"])) {
        if ($_COOKIE["theme"] === "Bright") {
            print('<link rel="stylesheet" href="css/theme-bright-main.css">');
            print('<link rel="stylesheet" href="css/theme-bright-fancySelect.css">');
            return;
        }
    }
    // default
    print('<link rel="stylesheet" href="css/theme-dark-main.css">');
    print('<link rel="stylesheet" href="css/theme-dark-fancySelect.css">');
}



// Returns the sign of a number.
function sign($n) {
    return ($n > 0) - ($n < 0);
}



// Send a mail. The default mail() command only works with sendmail, which is old and not installed everywhere.
function sendMail($toAddresses, $subject, $body) {
    if (defined("SMTP_HOST")) {
        $mail = new PHPMailer(true);
        $mail->isSMTP();                                            //Send using SMTP
        $mail->SMTPAuth   = true;                                   //Enable SMTP authentication
        $mail->SMTPSecure = PHPMailer::ENCRYPTION_SMTPS;            //Enable implicit TLS encryption
        $mail->Port       = SMTP_PORT;
        $mail->Host       = SMTP_HOST;
        $mail->Username   = SMTP_USER;
        $mail->Password   = SMTP_PASSWORD;
        $mail->setFrom(MAIL_TX_ADDRESS);

        // add recipients
        if (is_array($toAddresses)) {
            foreach ($toAddresses as $h) {
                $mail->addAddress($address);
            }
        } else {
            $mail->addAddress($toAddresses);
        }

        // add subject and body
        $mail->Subject = $subject;
        $mail->Body    = $body;

        // send it
        $mail->send();
    } else {
        mail($toAddresses, $subject, $body, "From: " . MAIL_TX_ADDRESS);
    }
}
