<?php
if(!defined("WEBSITE_PHP_DEF")) {
    header("HTTP/1.1 401 Unauthorized");
    die("Access denied");
}
require_once("config.php");

// create a global pdo object
if (DB_TYPE === "mysql") {
    // PDO::MYSQL_ATTR_FOUND_ROWS: make sure that an UPDATE operation which changes a value to the same value (i.e. 1 -> 1) returns that one row was affected
    $pdo = new PDO("mysql:unix_socket=" . DB_PATH . ";dbname=". DB_NAME . ";charset=utf8mb4", DB_USER, KICKER_DB_PW, array(PDO::MYSQL_ATTR_FOUND_ROWS => true));
}
elseif (DB_TYPE === "sqlite") {
    $pdo = new PDO('sqlite:' . DB_PATH);
}

$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
$pdo->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);

// store in GLOBALS such that we can always reference it
$GLOBALS['pdo'] = $pdo;