<?php
define("WEBSITE_PHP_DEF", true);
require_once("common.php");
checkAccess();
setCookiesFromUrl();
?>

<!DOCTYPE html>
<html lang="en-US">

<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
<link rel="shortcut icon" type="image/x-icon" href="pics/favicon.ico" />

<link rel="stylesheet" href="css/main.css">
<link rel="stylesheet" href="css/fancySelect.css">
<?php loadTheme() ?>

<script src="lib/kicker.js"></script>
<script src="lib/fancySelect.js"></script>

<!-- load chart.js with all related libs -->
<script src="lib/moment.js"></script>
<script src="lib/moment-de.js"></script>
<script src="lib/Chart.js"></script>
<script src="lib/hammer.js"></script>
<script src="lib/chartjs-plugin-zoom.js"></script>
<script src="lib/historyCanvas.js"></script>
</head>



<body>
<menu id="topnav"> </menu>

<div class="flex-container">
    <div> <h1 class=eloHistColA>Élő Score History</h1> </div>
    <div>
        <div id="player1"></div>
        <div id="player2"></div>
        <div id="player3"></div>
        <div id="player4"></div>
        <div id="player5"></div>
    </div>
    <div> <button onclick="historyCanvas.zoomAll(document.getElementById('eloHistCanvas'));">Zoom Out</button> </div>
    <div> <button onclick="historyCanvas.zoomLastMonth(document.getElementById('eloHistCanvas'));">Zoom Last Month</button> </div>
    <div> <canvas id="eloHistCanvas"></canvas> </div>
</div>



<script defer>
// how many players are selectable
const N_ELO_HIST_PLAYERS = 5;

// Fills the chart with fresh data.
function updateChart() {
    let players = [];
    let pNames = [];
    for (let i = 0; i < N_ELO_HIST_PLAYERS; i++) {
        // get player names from the selectors
        let pName = fancySelect.get(document.getElementById("player" + String(i+1))).toLowerCase();
        // store player objects
        if (pName != "-") {
            players.push(glblAllPlayers[pName]);
            pNames.push(pName);
        }
    }

    historyCanvas.fillChart(document.getElementById("eloHistCanvas"), players);

    // store the selection in the URL
    var newUrl = new URL(window.location);
    if (players.length > 0)
        newUrl.searchParams.set("players", pNames.join());
    else
        newUrl.searchParams.delete("players");
    window.history.pushState(null, "", newUrl);
}



function setupPage(ctx, data) {
    let pNames = [];
    // try to load players from URL
    if (ctx.args.players)
        pNames = decodeURIComponent(ctx.args.players).split(",");

    // try to push the player from the cookie
    let playerFromCookie = getCookie("player");
    if (playerFromCookie !== "" && !pNames.includes(playerFromCookie))
        pNames.unshift(playerFromCookie);

    // if no players were found or only the one from the cookie,
    // randomly pre select two more active players
    if (pNames.length == 0 || (pNames.length == 1 && playerFromCookie != "")) {
        // get all active players, exclude names which were given as URL parameters
        let randomPlayerNames = [...Object.keys(data.activePlayers)]; // copy
        randomPlayerNames = randomPlayerNames.filter(e => e !== playerFromCookie);
        randomPlayerNames = randomPlayerNames.filter(e => !pNames.includes(e));

        for (let i = 0; i < 2; i++)
            pNames.push(randomPlayerNames.splice(Math.floor(Math.random() * randomPlayerNames.length), 1)[0]);
    }

    // put all other players to "-"
    for (let i = pNames.length; i < N_ELO_HIST_PLAYERS; i++)
        pNames.push("-");

    let pSelItems = Object.keys(data.playersWithGames).map(x => capName(x));
    pSelItems.unshift("-");
    for (let i = 0; i < N_ELO_HIST_PLAYERS; i++) {
        let pSel = document.getElementById("player" + String(i+1));
        fancySelect.create(pSel, pSelItems);
        fancySelect.selectByText(pSel, capName(pNames[i]));
        fancySelect.setOnSelect(pSel, updateChart);
    }
}



window.onload = function() {
    setupTopNav();
    fetchData(["allPlayers", "eloDetails"]).then( function(data) {
        // store player as global variable, so that we can update the chart easily from the player selector callbacks
        glblAllPlayers = data.allPlayers;

        setupPage(getCtx(), data);
        historyCanvas.genChart(document.getElementById("eloHistCanvas"), N_ELO_HIST_PLAYERS);
        updateChart();
    });
}
</script>
</body>
</html>
