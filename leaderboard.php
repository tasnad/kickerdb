<?php
define("WEBSITE_PHP_DEF", true);
require_once("common.php");
checkAccess();
setCookiesFromUrl();
require_once("apiFunctions.php");
?>

<!DOCTYPE html>
<html lang="en-US">

<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
<link rel="shortcut icon" type="image/x-icon" href="pics/favicon.ico" />

<link rel="stylesheet" href="css/main.css">
<?php loadTheme() ?>

<script src="lib/kicker.js"></script>
<script src="lib/moment.js"></script>
</head>



<body>
<menu id="topnav"> </menu>

<div class="flex-container">
    <div> <h1>Ranking</h1> </div>
    <div> <table class="dataTable" id="tblBoard"></table> </div>

    <div> <h1>Quick Stats</h1> </div>
    <div> <table class="dataTable padCells">
        <tr>
            <td>Most Active Player(s) of the Week (games)</td>
            <td id="rankingActive"></td>
        </tr>
        <tr>
            <td>Player(s) of the Week (score)</td>
            <td id="playerOfTheWeek"></td>
        </tr>
        <tr>
            <td>Player(s) of the Day (score)</td>
            <td id="playerOfTheDay"></td>
        </tr>
    </table> </div>

    <div> <h1>Games Today</h1> </div>
    <div> <h3 id=noGamesToday>No Games Today.</h3> </div>
    <div> <table class="dataTable padCells" id="tblToday"></table> </div>

    <div> <h1>Games This Week</h1> </div>
    <div> <h3 id=noGamesWeek>No Games This Week.</h3> </div>
    <div> <table class="dataTable padCells" id="tblWeek"></table> </div>

    <div> <h1>Player in Row vs. Player in Column</h1> </div>
    <div> <table class="dataTable" id="tblVs"></table> </div>
</div>



<script defer>
// Populates the tables from game stats.
function genTables(data) {
    let tblBoard = document.getElementById("tblBoard");
    let tblToday = document.getElementById("tblToday");
    let noGamesToday = document.getElementById("noGamesToday");
    let noGamesWeek = document.getElementById("noGamesWeek");
    let tblVs = document.getElementById("tblVs");

    // hide table to disable rendering while tables are built
    tblBoard.hidden = true;
    tblVs.hidden = true;

    // clear tables
    while (tblBoard.rows.length > 0) tblBoard.deleteRow(0);
    while (tblVs.rows.length > 0) tblVs.deleteRow(0);

    // set up ranking numbers with their ordinal suffixes
    function updateRankColumn(tbl) {
        for(let r = 1; r < tbl.rows.length; r++) {
            tbl.rows[r].cells[0].innerText = addOrdinalSuffix(r);
        }
    }

    // *** Column Settings *** //
    let cols = {
        "rank":         {index:  0, title: "Rank",                 tooltip: "Ranking of the player, depending on which column you use for sorting"},
        "player":       {index:  1, title: "Player",               tooltip: "Name of the player"},
        "eloTotal":     {index:  2, title: "Élő Total",            tooltip: "Élő score for of the player: total points - penalty"},
        "eloDelta":     {index:  3, title: "Élő Δ",                tooltip: "Élő point difference to next better player"},
        "eloLast":      {index:  4, title: "Élő Last",             tooltip: "Élő score the player won or lost in the last game"},
        "eloPenalty":   {index:  5, title: "Élő Penalty",          tooltip: "Total penalties due to inactivity (between two games or since the last game), contained in Élő total"},
        "noob":         {index:  6, title: "Newcomer Bonus",       tooltip: "If a player has opted for starting with a newcomer bonus, this shows how many games he has left and which maximum Élő his games have"},
        "totalScore":   {index:  7, title: "Total Score",          tooltip: "Sum of achieved goals - sum of conceded goals"},
        "averageScore": {index:  8, title: "Average Score",        tooltip: "Total score / number of games. Only valid if number of games > 5."},
        "winRatio":     {index:  9, title: "Win Ratio [%]",        tooltip: "Wins / (Wins + Losses). Only valid if number of games > 5."},
        "gamesPlayed":  {index: 10, title: "Games Played",         tooltip: "Total number of games the player was involved"},
        "gamesWon":     {index: 11, title: "Games Won",            tooltip: "Number of games the player won"},
        "gamesLost":    {index: 12, title: "Games Lost",           tooltip: "Number of games the player lost"},
        "totalGoalsP":  {index: 13, title: "Total Goals Shot",     tooltip: "Number of goals the player scored (together with its partner)"},
        "totalGoalsN":  {index: 14, title: "Total Goals Suffered", tooltip: "Number of goals the player lost (together with its partner)"},
    };

    // create table head
    let th = document.createElement("tr");
    let tblCells = {};
    for (let [colName, colMeta] of Object.entries(cols)) {
        let tblCell = addCell(th, colMeta.title, true);
        tblCell.title = colMeta.tooltip;
        if (colName != "rank")
            tblCell.className = "monocolorLink";
        let curColIndx = colMeta.index; // needed to pass the *current* value of colMeta.index to the sort function
        tblCell.onclick = function() { sortTable({tbl: tblBoard, column: curColIndx, numeric: true, ascending: false}); updateRankColumn(tblBoard); };
        tblCells[colName] = tblCell;
    }
    // override column specific stuff
    // alphabetic sorting of player names
    tblCells["player"].onclick = function() { sortTable({tbl: tblBoard, column: cols.player.index, numeric: false, ascending: true}); updateRankColumn(tblBoard); };
    tblBoard.appendChild(th);

    // add data
    let playerFromCookie = getCookie("player");
    for (let [pName, p] of Object.entries(data.activePlayers)) {
        let tr = document.createElement("tr");
        for (let i = 0; i < Object.keys(cols).length; i++)
            addCell(tr, "-");

        let a = document.createElement("a");
        a.innerText = capName(pName);
        a.href = "playerStats.php?player=" + pName;
        if (playerFromCookie === pName)
            a.className = "monocolorLinkHighlight";
        else
            a.className = "monocolorLink";

        tr.cells[cols.player.index].innerText = "";
        tr.cells[cols.player.index].appendChild(a);

        tr.cells[cols.eloTotal.index].innerText    = p.eloTotal.toFixed(1);
        tr.cells[cols.eloDelta.index].innerText    = p.eloDiffBetter.toFixed(1);
        tr.cells[cols.eloLast.index].innerText     = p.eloLast.toFixed(1);
        tr.cells[cols.eloPenalty.index].innerText  = p.eloPenalty.toFixed(1);
        if (p.newcomerBonusGranted == 1) {
            if (p.newcomerBonusGamesRemain == 0)
                tr.cells[cols.noob.index].innerText = `(bonus over, was ${p.newcomerBonus/100 * data.config.eloKRegular} Élő)`;
            else if (p.newcomerBonusGamesRemain == 1)
                tr.cells[cols.noob.index].innerText = `1 game with ${p.newcomerBonus/100 * data.config.eloKRegular} Élő`;
            else
                tr.cells[cols.noob.index].innerText = `${p.newcomerBonusGamesRemain} games with ${p.newcomerBonus/100 * data.config.eloKRegular} Élő`;
        }
        else {
            tr.cells[cols.noob.index].innerText    = "(no bonus)";
        }
        tr.cells[cols.totalScore.index].innerText  = p.score;
        // if there is no win ratio so far, leave the cell as "-"
        if (isFinite(p.winRatio)) {
            tr.cells[cols.averageScore.index].innerText = p.efficiency.toFixed(2);
            tr.cells[cols.winRatio.index].innerText     = (p.winRatio * 100).toFixed(2);
        }
        tr.cells[cols.gamesPlayed.index].innerText = p.gamesTotal;
        tr.cells[cols.gamesWon.index].innerText    = p.gamesWon;
        tr.cells[cols.gamesLost.index].innerText   = p.gamesLost;
        tr.cells[cols.totalGoalsP.index].innerText = p.totalGoalsP;
        tr.cells[cols.totalGoalsN.index].innerText = p.totalGoalsN;

        tblBoard.appendChild(tr);
    }

    // coloring of the columns
    for (let [colName, colMeta] of Object.entries(cols)) {
        if (colName === "eloTotal")
            highlightSpectrum({tbl: tblBoard, lims: {cMin: colMeta.index, cMax: colMeta.index, rMin: 1, rMax: Infinity}});
        else if (colName === "eloLast")
            highlightSpectrum({tbl: tblBoard, minValOverride: <?php print(-0.8 * ELO_K_REGULAR) ?>, maxValOverride: <?php print(0.8 * ELO_K_REGULAR) ?>, lims: {cMin: colMeta.index, cMax: colMeta.index, rMin: 1, rMax: Infinity}});
        else if (colName === "noob")
            highlightRegex({tbl: tblBoard, patternMatch: "Yes", lims: {cMin: colMeta.index, cMax: colMeta.index, rMin: 1, rMax: Infinity}});
        else if (colName === "totalScore")
            highlightSpectrum({tbl: tblBoard, lims: {cMin: colMeta.index, cMax: colMeta.index, rMin: 1, rMax: Infinity}});
        else if (colName === "gamesLost")
            highlightMinMax({tbl: tblBoard, lims: {cMin: colMeta.index, cMax: colMeta.index, rMin: 1, rMax: Infinity}, "invert": true}); // invert colors
        else if (colName === "averageScore" || colName === "winRatio" || colName === "gamesPlayed" || colName === "gamesWon" || colName === "totalGoalsP")
            highlightMinMax({tbl: tblBoard, lims: {cMin: colMeta.index, cMax: colMeta.index, rMin: 1, rMax: Infinity}});
    }

    sortTable({tbl: tblBoard, column: cols.eloTotal.index, numeric: true, ascending: false});
    updateRankColumn(tblBoard);



    // today's games table
    if (data.nGamesToday > 0) {
        gamesTable(tblToday, data.gamesToday, data.config);
        noGamesToday.hidden = true;
    }
    else {
        noGamesToday.hidden = false;
    }

    // week games table
    if (data.nGamesWeek > 0) {
        gamesTable(tblWeek, data.gamesWeek, data.config);
        noGamesWeek.hidden = true;
    }
    else {
        noGamesWeek.hidden = false;
    }


    // *** Table With Opponent Details *** //
    // create elements with names and 0 score
    let plName2TblRC = {}; // player name -> table row/column index dict
    // start with creating the table head
    th = document.createElement("tr");

    addCell(th, "Player", true);
    let curCol = 1;
    // find the selected player's column
    let pCol = 0;
    for (let [pName, p] of Object.entries(data.playersWithGames))
        if (playerFromCookie === pName) break; else pCol += 1;
    for (let [pName, p] of Object.entries(data.playersWithGames)) {
        // header
        addCell(th, capName(pName), true);
        plName2TblRC[p.name] = curCol++;

        // higher rows
        let tr = document.createElement("tr");
        if (playerFromCookie === pName) tr.classList.add("highlighted");

        addCell(tr, capName(pName));
        for (let i = 0; i < data.nPlayersWithGames; i++) {
            let c = addCell(tr, "-");
            if (i == pCol) c.classList.add("highlighted");
        }
        addCell(tr, capName(pName));
        tblVs.appendChild(tr);
    }
    addCell(th, "Player", true);
    tblVs.insertBefore(th, tblVs.rows[0]);

    // add scores
    // increments the value in the cell defined by row r and column c in table tbl by the given ammount inc.
    function addTo(tbl, r, c, inc) {
        let e = tbl.rows[r].cells[c];
        if (e.innerText === "-")
            e.innerText = 0;
        e.innerText = Number(e.innerText) + inc;
    }

    for (let g of data.allGames) {
        let a1 = plName2TblRC[g.playerA1], a2 = plName2TblRC[g.playerA2], b1 = plName2TblRC[g.playerB1], b2 = plName2TblRC[g.playerB2];
        let s = g.scoreA - g.scoreB;

        if (a1 === a2 && b1 === b2) {
            // 1 on 1
            addTo(tblVs, a1, b1,  s);
            addTo(tblVs, b1, a1, -s);
        }
        else if (a1 === a2) {
            // 1 on 2
            addTo(tblVs, a1, b1,  s);
            addTo(tblVs, b1, a1, -s);

            addTo(tblVs, a1, b2,  s);
            addTo(tblVs, b2, a1, -s);
        }
        else if (b1 === b2) {
            // 2 on 1
            addTo(tblVs, a1, b1,  s);
            addTo(tblVs, b1, a1, -s);

            addTo(tblVs, a2, b1,  s);
            addTo(tblVs, b1, a2, -s);
        }
        else {
            // 2 on 2
            addTo(tblVs, a1, b1,  s);
            addTo(tblVs, b1, a1, -s);

            addTo(tblVs, a1, b2,  s);
            addTo(tblVs, b2, a1, -s);

            addTo(tblVs, a2, b1,  s);
            addTo(tblVs, b1, a2, -s);

            addTo(tblVs, a2, b2,  s);
            addTo(tblVs, b2, a2, -s);
        }
    }

    highlightSpectrum({tbl: tblVs, lims: {cMin: 1, cMax: data.nPlayersWithGames+1, rMin: 1, rMax: Infinity}});



    // unhide tables
    tblBoard.hidden = false;
    if (data.nGamesToday > 0)
        tblToday.hidden = false;
    tblVs.hidden = false;
}



function updateStats(data) {
    // best player(s) of the day and week
    let pd = [ data.rankingToday[0] ], pw = [ data.rankingWeek[0] ], pa = [ data.rankingActive[0] ];
    for (p of data.rankingToday.slice(1)) {
        if(p.score == pd.slice(-1)[0].score)
            pd.push(p);
        else
            break;
    }
    for (p of data.rankingWeek.slice(1)) {
        if(p.score == pw.slice(-1)[0].score)
            pw.push(p);
        else
            break;
    }
    for (p of data.rankingActive.slice(1)) {
        if(p.gamesTotal == pa.slice(-1)[0].gamesTotal)
            pa.push(p);
        else
            break;
    }



    let infoTxt = "";
    if (pa[0].gamesTotal == 0) {
        infoTxt += "-";
    }
    else {
        for (p of pa)
            infoTxt += capName(p.name) + ": " + p.gamesTotal + ", ";
        infoTxt = infoTxt.slice(0, -2);
    }
    document.getElementById("rankingActive").innerText = infoTxt;

    infoTxt = "";
    if (pw[0].score == 0) {
        infoTxt += "-";
    }
    else {
        for (p of pw)
            infoTxt += capName(p.name) + ": " + p.score + ", ";
        infoTxt = infoTxt.slice(0, -2);
    }
    document.getElementById("playerOfTheWeek").innerText = infoTxt;

    infoTxt = "";
    if (pd[0].score == 0) {
        infoTxt += "-";
    }
    else {
        for (p of pd)
            infoTxt += capName(p.name) + ": " + p.score + ", ";
        infoTxt = infoTxt.slice(0, -2);
    }
    document.getElementById("playerOfTheDay").innerText = infoTxt;
}



window.onload = function() {
    setupTopNav();
    fetchData(["config", "allPlayers", "allGames", "gamesToday", "gamesWeek", "rankingToday", "rankingWeek", "rankingActive"], "rankSortkey=score").then( function(data) {
        updateStats(data);
        genTables(data);
    });
}
</script>
</body>
</html>
