// Creates fancy select inputs, as a replacement for <select> elements.
// Usage:
// <div id="mySel"></div>
//
// fancySelect.create(document.getElementbyId("mySel"));
// fancySelect.add("mySel", ["Cars", "Trains", "Planes"]);
// fancySelect.selectByText("mySel", "Trains");
// let selection = fancySelect.get("mySel");
fancySelect = {
    // Creates a fancy selector in the given div.
    // If "items" is given, it will be passed to add() to fill the selector.
    // moveTop: If true, the menu will be moved to the top of the current
    // viewport, whenever it is clicked. Otherwise, the menu position is fixed
    // to the top of the page
    // topOffset: how much to move the menu from the top if moveTop is true.
    create: function(mainDiv, items=null, moveTop=false, topOffset=80) {
        // populate the div
        let btnOpen = document.createElement("button");
        // floatDiv holds the menu if it is open
        let floatDiv = document.createElement("div");
        let btnCloseTop = document.createElement("button");
        let btnCloseBottom = document.createElement("button");
        let uList = document.createElement("ul");
        // show typed letters at the bottom
        let keyBufP  = document.createElement("p");
        mainDiv.appendChild(btnOpen);
        mainDiv.appendChild(floatDiv);
        floatDiv.appendChild(btnCloseTop);
        floatDiv.appendChild(uList);
        floatDiv.appendChild(keyBufP);
        floatDiv.appendChild(btnCloseBottom);

        mainDiv.className  = "fancySelectContainer";
        btnOpen.className  = "btnOpen";
        floatDiv.className = "floatDiv";
        uList.className    = "uList";
        btnCloseTop.innerText = "❌";
        btnCloseBottom.innerText = "❌";

        // store some references in the main div
        mainDiv.floatDiv      = floatDiv;
        mainDiv.btnOpen       = btnOpen;
        mainDiv.uList         = uList;
        mainDiv.keyBufP       = keyBufP;
        mainDiv.moveTop       = moveTop;
        mainDiv.menuTopOffset = topOffset;
        mainDiv.onSelectFun   = null;
        mainDiv.onClickFun    = null;
        mainDiv.selId         = null;
        mainDiv.keyBuf        = "";

        // toggle the menu if the button was clicked
        btnOpen.addEventListener("click", function(e) {
            e.preventDefault();
            fancySelect.toggle(mainDiv);
        });

        // close the menu with the close buttons
        btnCloseTop.addEventListener("click", function(e) {
            e.preventDefault();
            fancySelect.close(mainDiv);
        });
        btnCloseBottom.addEventListener("click", function(e) {
            e.preventDefault();
            fancySelect.close(mainDiv);
        });

        // called if an entry is selected
        mainDiv.entryClickFun = function(e) {
            e.preventDefault();
            fancySelect.highlightById(mainDiv, this.elemId);
            fancySelect.acceptHighlighted(mainDiv);

            // call the onclick function if there is one
            if (mainDiv.onClickFun)
                mainDiv.onClickFun();
        }

        // close menu if there is a click outside of this fancy select object
        document.addEventListener("mousedown", function(e) {
            if (!mainDiv.contains(e.target))
                fancySelect.close(mainDiv);
        });
        btnOpen.onkeydown  = function(e) {fancySelect.keyDown(mainDiv, e)};

        btnCloseTop.onkeydown = function(e) {fancySelect.keyDown(mainDiv, e)};
        btnCloseBottom.onkeydown = function(e) {fancySelect.keyDown(mainDiv, e)};

        // add the items if provided
        if (items)
            fancySelect.add(mainDiv, items);
    },

    // Returns whether the menu is open.
    isOpen: function(mainDiv) {
        return mainDiv.floatDiv.classList.contains("open");
    },
    // Shows the menu.
    open: function(mainDiv) {
        if (mainDiv.moveTop){
            // unfortunately this always gives 0 on firefox for android.
            // It seems there is no way around.
            let scrollTop = window.scrollY || window.pageYOffset || document.body.scrollTop + (document.documentElement && document.documentElement.scrollTop || 0);
            mainDiv.floatDiv.style.top = String(scrollTop + mainDiv.menuTopOffset) + "px";
        }

        mainDiv.floatDiv.classList.add("open");
        mainDiv.btnOpen.classList.add("open");
        // focus the list for key presses
        mainDiv.btnOpen.focus({preventScroll: true});

        mainDiv.keyBuf = "";
        fancySelect.updateKeybufP(mainDiv);
    },
    // Closes the menu.
    close: function(mainDiv) {
        mainDiv.floatDiv.classList.remove("open");
        mainDiv.btnOpen.classList.remove("open");
    },
    // Toggles the menu.
    toggle: function(mainDiv) {
        if (fancySelect.isOpen(mainDiv))
            fancySelect.close(mainDiv);
        else
            fancySelect.open(mainDiv);
    },



    // Adds items to the selector.
    // E.g. ["Cars", "Trains", "Planes"]
    add: function(mainDiv, items) {
        // create a new entry for each element in items
        let elemId = fancySelect.nItems(mainDiv); // next elemID
        items.forEach(function(iTxt) {
            let e = document.createElement("li");
            e.className = "option";

            let a = document.createElement("a");
            a.innerHTML = iTxt;
            a.setAttribute("href", "#");
            a.addEventListener("mousedown", mainDiv.entryClickFun, false);

            a.elemId = elemId;
            elemId += 1;

            e.appendChild(a);
            e.lnk = a; // store for convenience
            mainDiv.uList.appendChild(e);
        });

        // highlight the last
        fancySelect.selectByText(mainDiv, items[items.length - 1]);
    },

    // Returns the currently selected item's text.
    get: function(mainDiv) {
        return mainDiv.btnOpen.innerHTML;
    },

    // Returns how many items are in the selector.
    nItems: function(mainDiv) {
        return mainDiv.uList.children.length;
    },


    // Highlights the item with the given elemId.
    highlightById: function(mainDiv, elemId) {
        let entries = mainDiv.uList.children;

        if (elemId < 0 || elemId >= entries.length)
            return;

        // unhighlight old selection
        if (mainDiv.selId != null)
            entries[mainDiv.selId].lnk.classList.remove("active");

        entries[elemId].lnk.classList.add("active");
        mainDiv.selId = elemId;
    },

    // Advances the highlight by the given steps (may be nagative).
    // wrap: controls whether to wrap around.
    highlightNext: function(mainDiv, step=1, wrap=true) {
        let entries = mainDiv.uList.children;

        let N = entries.length;
        let newIndx = mainDiv.selId + step;
        newIndx = ((newIndx % N) + N) % N; // modulo which also works for negative numbers
        fancySelect.highlightById(mainDiv, newIndx);
    },

    highlightFromMatch: function(mainDiv, matchStr) {
        let matchLower = matchStr.toLowerCase();

        // see if we find an entry which starts with the match
        for (let e of mainDiv.uList.children) {
            if (e.lnk.innerHTML.toLowerCase().startsWith(matchLower)) {
                fancySelect.highlightById(mainDiv, e.lnk.elemId);
                return;
            }
        }

        // if not, try to match the whole text
        for (let e of mainDiv.uList.children) {
            if (e.lnk.innerHTML.toLowerCase().match(matchLower) != null) {
                fancySelect.highlightById(mainDiv, e.lnk.elemId);
                return;
            }
        }
    },



    // Closes the menu and sets the text of the selected element as the main text.
    acceptHighlighted: function(mainDiv) {
        if (mainDiv.selId == null)
            return;

        let entries = mainDiv.uList.children;
        mainDiv.btnOpen.innerHTML = entries[mainDiv.selId].lnk.innerHTML;

        fancySelect.close(mainDiv);
    },

    // Selects the item with the given text.
    // Selecting means highlighting and closing the menu.
    // Returns true or false if the selection was successful.
    selectByText: function(mainDiv, itemTxt) {
        for (let e of mainDiv.uList.children) {
            if (e.lnk.innerHTML == itemTxt) {
                fancySelect.selectById(mainDiv, e.lnk.elemId);
                return true;
            }
        };
        return false;
    },

    // Selects the item with the given id.
    // Selecting means highlighting and closing the menu.
    selectById: function(mainDiv, elemId) {
        if (elemId < 0 || elemId >= fancySelect.nItems(mainDiv))
            return;

        fancySelect.highlightById(mainDiv, elemId);
        fancySelect.acceptHighlighted(mainDiv);

        // call the onselect function if there is one
        if (mainDiv.onSelectFun)
            mainDiv.onSelectFun();
    },

    // Selects a random item.
    selectRandom: function(mainDiv) {
        fancySelect.selectById(mainDiv, Math.floor(Math.random() * (fancySelect.nItems(mainDiv) - 1)));
    },



    // Sets the event which should be triggered when a selection is made.
    // onClick: set up as event if the user clicks on en entry
    // onSelect: set up as event if fancySelect.selectByText is called
    setOnSelect: function(mainDiv, fun, onClick=true, onSelect=false) {
        if (onClick)  mainDiv.onClickFun  = fun;
        if (onSelect) mainDiv.onSelectFun = fun;
    },



    // Updates the "filter" <p> element from the keybuf's contents.
    updateKeybufP: function(mainDiv) {
        if (mainDiv.keyBuf.length > 0)
            mainDiv.keyBufP.innerHTML = "Filter: " + mainDiv.keyBuf;
        else
            mainDiv.keyBufP.innerHTML = "";
    },

    // Handle keypresses on the button.
    keyDown: function(mainDiv, e) {
        let isOpen = fancySelect.isOpen(mainDiv);

        if (isOpen && e.keyCode === 27) { // escape
            fancySelect.close(mainDiv);
            e.preventDefault();
        }
        else if (isOpen && e.keyCode === 13) { // return
            fancySelect.acceptHighlighted(mainDiv);
            // call the onclick function if there is one
            if (mainDiv.onClickFun)
                mainDiv.onClickFun();
            e.preventDefault();
        }
        else if (isOpen && e.keyCode === 8) { // backspace
            if (mainDiv.keyBuf.length > 0)
                mainDiv.keyBuf = mainDiv.keyBuf.slice(0, -1);
            e.preventDefault();
        }
        else if (isOpen && e.keyCode === 9) { // tab
            if (e.shiftKey)
                fancySelect.highlightNext(mainDiv, -1);
            else
                fancySelect.highlightNext(mainDiv);
            // reset the keybuf
            mainDiv.keyBuf = "";
            e.preventDefault();
        }
        else if (isOpen && (e.keyCode == 37 || e.keyCode == 38)) { // left or up
            fancySelect.highlightNext(mainDiv, -1);
            // reset the keybuf
            mainDiv.keyBuf = "";
            e.preventDefault();
        }
        else if (isOpen && (e.keyCode == 39 || e.keyCode == 40)) { // right or down
            fancySelect.highlightNext(mainDiv);
            // reset the keybuf
            mainDiv.keyBuf = "";
            e.preventDefault();
        }
        else if (
            (e.keyCode >= 65 && e.keyCode <= 90) || // a-z
            (e.keyCode >= 48 && e.keyCode <= 57) || // 0-9
            (e.keyCode == 61)    // =
        ) {
            // open the menu if not opened yet
            if (!isOpen) fancySelect.open(mainDiv);

            mainDiv.keyBuf += String.fromCharCode(e.keyCode).toLowerCase();
            fancySelect.highlightFromMatch(mainDiv, mainDiv.keyBuf);
            e.preventDefault();
        }

        fancySelect.updateKeybufP(mainDiv);
    }
}
