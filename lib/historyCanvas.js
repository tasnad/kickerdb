// Creates a Chart.js chart.
historyCanvas = {
// Creates and fills the chart.
// canvas: the HTML5 canvas in the document to puth the chart in.
// nPlayers: how many datasets to plot (at maximum). Make sure that enough CSS color ("eloHistCol<N>") exist.
genChart: function(canvas, nPlayers) {
    // create the chart with empty data
    let chartCfg = {
        type: 'line',
        data: {
            datasets: []
        },
        options: {
            title: {
                display: false,
            },
            scales: {
                xAxes: [
                    {
                        type: 'time',
                        time: {
                            displayFormats: {
                                millisecond: 'ddd DD.MM.YYYY HH:mm:ss',
                                second: 'ddd DD.MM.YYYY HH:mm:ss',
                                minute: 'ddd DD.MM.YYYY HH:mm:ss',
                                hour: 'ddd DD.MM.YYYY HH:mm:ss',
                                day: 'ddd DD.MM.YYYY HH:mm:ss',
                                week: 'ddd DD.MM.YYYY',
                                month: 'ddd DD.MM.YYYY',
                                quarter: 'ddd DD.MM.YYYY',
                                year: 'ddd DD.MM.YYYY',
                            },
                            tooltipFormat: 'dddd, DD.MM.YYYY HH:mm:ss'
                        },
                        scaleLabel: {
                            display: true,
                            labelString: 'Time'
                        },
                        distribution: 'linear',
                        ticks: {
                            minRotation: 45,
                            maxRotation: 90,
                        }
                    }
                ],
                yAxes: [
                    {
                        type: 'linear',
                        display: true,
                        scaleLabel: {
                            display: true,
                            labelString: 'Points',
                        },
                    }
                ]
            },
            plugins: {
                zoom: {
                    zoom: {
                        enabled: true,
                        drag: false,
                        mode: 'x'
                    },
                    pan: {
                        enabled: true,
                        mode: 'x'
                    },
                },
            },
            tooltips: {
                mode: 'nearest',
                callbacks: {
                    label: function(tooltipItem, data) {
                        let dSet = data.datasets[tooltipItem.datasetIndex];
                        let gameDetails = dSet.data[tooltipItem.index].gameDetails;
                        if (gameDetails) {
                            return [
                                // players and game score
                                `${capName(gameDetails["playerA1"])} + ${capName(gameDetails["playerA2"])} ${gameDetails["scoreA"]} : ${gameDetails["scoreB"]} ${capName(gameDetails["playerB1"])} + ${capName(gameDetails["playerB2"])}`,
                                // scores
                                "Elő only this game:  ±" + gameDetails.eloGame.toFixed(2),
                                "Elő after this game: " + tooltipItem.value,
                                gameDetails.time
                            ];
                        }
                        else {
                            return "Now, incl. penalties since last game: " + tooltipItem.value;
                        }
                    }
                }
            },
            legend: {
                labels: {
                    // filter out hidden datasets
                    filter: function(item, chart) {
                        return !item.hidden;
                    }
                }
            },
            maintainAspectRatio: false,
            responsive: true
        }
    };

    // get colors from CSS
    let cols = [];
    let preClass = canvas.className;
    for (let i = 0; i < nPlayers; i++) {
        // use the canvas as dummy
        canvas.className = "eloHistCol" + String(i+1);
        cols.push(getComputedStyle(canvas).color);
    }
    canvas.className = preClass;

    // add a dataset for each player
    for(let i = 0; i < nPlayers; i++){
        chartCfg.data.datasets.push(
            {
                label: "",
                fill: false,
                data: [],
                borderColor: cols[i],
                backgroundColor: cols[i],
                pointBorderColor: cols[i],
                pointBackgroundColor: cols[i],
                lineTension: 0,
                pointBorderWidth: 2,
                hidden: true,
            }
        );
    }


    // create the chart and store a reference in the parent canvas
    let chart = new Chart(canvas, chartCfg);
    canvas.chart = chart;
},



// Fills the chart with fresh data.
// canvas: the HTML5 canvas in the document to puth the chart in.
// players: a list of player objects (from the PHP API) with players to plot.
fillChart: function(canvas, players) {
    // hide the canvas to be more efficient
    let preDisp = canvas.style.display;
    canvas.style.display = "none";

    // convert and append data
    let chart = canvas.chart;
    let nPlayers = players.length;

    for(let i = 0; i < nPlayers; i++) {
        // clear dataset
        chart.data.datasets[i].data = [];

        // enter data
        let p = players[i];
        // name
        chart.data.datasets[i].label = capName(p.name);
        // data points
        p.eloHistory.forEach(function(e) {
            chart.data.datasets[i].data.push({
                t: moment.utc(e["time"]).local().format('YYYY-MM-DD HH:mm:ss'),
                y: e["eloTotal"].toFixed(1),
                gameDetails: e
            });
        });
        // append a last point with the player's current elo
        chart.data.datasets[i].data.push({
            t: moment().local().format('YYYY-MM-DD HH:mm:ss'),
            y: p.eloTotal.toFixed(1),
            gameDetails: null
        });
        // make this dataset visible
        chart.data.datasets[i].hidden = false;
    }

    // hide empty datasets
    for(let i = nPlayers; i < chart.data.datasets.length; i++)
        chart.data.datasets[i].hidden = true;

    // let the chart display the fresh data
    chart.update();

    // unhide it
    canvas.style.display = preDisp;
},



clearChart: function(canvas) {
    let chart = canvas.chart;
    for(let i = 0; i < chart.data.datasets.length; i++) {
        chart.data.datasets[i].data = [];
        chart.data.datasets[i].hidden = true;
    }
    chart.update();
},



zoomLastMonth: function(canvas) {
    let chart = canvas.chart;
    let scale = chart.scales["x-axis-0"];
    var timeOptions = scale.options.time;
    delete timeOptions.max;
    timeOptions.min = moment.utc().add(-1, 'M').valueOf();
    chart.update();
},


zoomAll: function(canvas) {
    let chart = canvas.chart;
    let scale = chart.scales["x-axis-0"];
    var timeOptions = scale.options.time;
    delete timeOptions.min;
    delete timeOptions.max;
    chart.update();
}
}
