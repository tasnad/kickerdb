// Returns the current page.
// For example, "hello.php" will be returned if we are on https://example.com/sub/hello.php?q=2
function getCurrentPage() {
    return window.location.pathname.split('/').pop();
}



// Fills up the top navigation menu with.
// Page can be used to force a specific entry to be highlighted, instead of the
// current page.
function setupTopNav(page=getCurrentPage()) {
    let menuEntries = [
        ["⚙",            "settings.php"   ],
        ["🔑",           "admin.php"      ],
        ["Add Game",     "addGame.php"    ],
        ["Leaderboard",  "leaderboard.php"],
        ["Élő Charts",   "eloCharts.php"  ],
        ["All Games",    "allGames.php"   ],
        ["Player Stats", "playerStats.php"],
        ["Player Games", "playerGames.php"],
        ["Kicker Rules", "rules.php"      ],
    ];

    // create all entries and set the current page as active
    let m = document.getElementById("topnav");
    menuEntries.forEach(function(e){
        let l = document.createElement("li");
        let a = document.createElement("a");
        a.innerText = e[0];
        a.href      = e[1];
        a.className = "monocolorLink";
        if (page == e[1])
            a.className += " active";
        l.appendChild(a);
        m.appendChild(l);
    });
}



// returns whether n is a number
function isNumeric(n) {
  return !isNaN(parseFloat(n)) && isFinite(n);
}

// Capitalizes the first letter of the given name. If it Starts with "z_", the name is also capitalized.
const capName = (s) => {
    if (typeof s !== 'string')
        return '';
    else
        if(s.startsWith("z_"))
            return "Z_" + s.charAt(2).toUpperCase() + s.slice(3);
        else
            return s.charAt(0).toUpperCase() + s.slice(1);
}


// Detects whether local or session storage is available and can be used.
// type may be 'localStorage' or 'sessionStorage'
function storageAvailable(type) {
    let storage;
    try {
        storage = window[type];
        let x = '__storage_test__';
        storage.setItem(x, x);
        storage.removeItem(x);
        return true;
    }
    catch(e) {
        return e instanceof DOMException && (
            // everything except Firefox
            e.code === 22 ||
            // Firefox
            e.code === 1014 ||
            // test name field too, because code might not be present
            // everything except Firefox
            e.name === 'QuotaExceededError' ||
            // Firefox
            e.name === 'NS_ERROR_DOM_QUOTA_REACHED') &&
            // acknowledge QuotaExceededError only if there's something already stored
            (storage && storage.length !== 0);
    }
}



function setCookie(cookieName, cookieValue, tld, cookiePath, expiresDays=null) {
    let expires = "";
    if (expiresDays) {
        let d = new Date();
        d.setTime(d.getTime() + (expiresDays * 24*60*60*1000));
        expires = "expires="+ d.toUTCString();
    }
    document.cookie = `${cookieName}=${cookieValue}; ${expires}; path=/${cookiePath}; samesite=strict; domain=${tld}; secure`;
}

function getCookie(cookieName) {
  var name = cookieName + "=";
  var decodedCookie = decodeURIComponent(document.cookie);
  var ca = decodedCookie.split(';');
  for(var i = 0; i < ca.length; i++) {
    var c = ca[i];
    while (c.charAt(0) == ' ') {
      c = c.substring(1);
    }
    if (c.indexOf(name) == 0) {
      return c.substring(name.length, c.length);
    }
  }
  return "";
}

function deleteCookie(cookieName, tld, cookiePath) {
    document.cookie = `${cookieName}=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/${cookiePath}; domain=${tld}`;
}



// Adds suffixes to numbers, like 1 -> "1st", 2 -> "2nd" etc.
function addOrdinalSuffix(i) {
    let j = i % 10,
        k = i % 100;
    if (j == 1 && k != 11) {
        return i + "st";
    }
    if (j == 2 && k != 12) {
        return i + "nd";
    }
    if (j == 3 && k != 13) {
        return i + "rd";
    }
    return i + "th";
}


/**
 * Converts an an RGB color in 0..255 to a hsl color in h: 0..360, s: 0..100, l:0..100.
 * From https://css-tricks.com/converting-color-spaces-in-javascript.
 * Returns a map with entries "h", "s", and "l"..
 */
function rgbToHsl(r, g, b) {
    // Make r, g, and b fractions of 1
    r /= 255;
    g /= 255;
    b /= 255;

    // Find greatest and smallest channel values
    let cmin = Math.min(r,g,b),
        cmax = Math.max(r,g,b),
        delta = cmax - cmin,
        h = 0,
        s = 0,
        l = 0;

    // Calculate hue
    // No difference
    if (delta == 0)
      h = 0;
    // Red is max
    else if (cmax == r)
      h = ((g - b) / delta) % 6;
    // Green is max
    else if (cmax == g)
      h = (b - r) / delta + 2;
    // Blue is max
    else
      h = (r - g) / delta + 4;

    h = Math.round(h * 60);

    // Make negative hues positive behind 360°
    if (h < 0)
        h += 360;
        // Calculate lightness
    l = (cmax + cmin) / 2;

    // Calculate saturation
    s = delta == 0 ? 0 : delta / (1 - Math.abs(2 * l - 1));

    // Multiply l and s by 100
    s = +(s * 100).toFixed(1);
    l = +(l * 100).toFixed(1);

    return {"h": h, "s": s, "l": l};
}



// Find a player's ID from his name.
// Returns -1 if it fails.
function findPlayerId(uName, players) {
    try {
        return players[uName].id;
    }
    catch
    {
        return -1;
    }
}
// Find a player's name from his ID.
function findPlayerName(uId, players) {
    for (const [pName, p] of Object.entries(players))
        if (p.id === uId)
            return pName;
    return null;
}



// Returns up a small storage with useful stuff.
function getCtx() {
    let ctx = {};

    // check if we have a local and session storage we can access
    ctx.localStorageAvailable   = storageAvailable('localStorage');
    ctx.sessionStorageAvailable = storageAvailable('sessionStorage');

    // find the base URL and store it for later reference
    ctx.baseUrl = window.location.href.split("?")[0].split("/").slice(0, -1).join("/");

    // store the query args
    ctx.args = {};
    window.location.search.slice(1).split("&").forEach( function(item) {
        pts = item.split("=");
        ctx.args[pts[0]] = pts[1];
    });

    return ctx;
}

// Fetches data from the server.
// queries: determines what data is loaded. For example ["singleGames", "players", "eloDetails"].
// params: appended to the URL
async function fetchData(queries=[], params="") {
    let baseUrl = window.location.href.split("?")[0].split("/").slice(0, -1).join("/");
    let res = await fetch(baseUrl + "/api.php?op=get_data&queries=" + queries.join(",") + "&" + params);
    if (!res.ok) throw Error(res.status + res.statusText);
    let apiRet = await res.json();

    // extract stuff
    let ret = {};
    ret.config          = apiRet.config;
    ret.doubleGames     = apiRet.doubleGames;
    ret.singleGames     = apiRet.singleGames;
    ret.allGames        = apiRet.allGames;
    ret.gamesToday      = apiRet.gamesToday;
    ret.gamesWeek       = apiRet.gamesWeek;
    ret.allPlayers      = apiRet.allPlayers;
    ret.playersToday    = apiRet.playersToday;
    ret.playersWeek     = apiRet.playersWeek;
    ret.rankingToday    = apiRet.rankingToday;
    ret.rankingWeek     = apiRet.rankingWeek;
    ret.rankingActive   = apiRet.rankingActive;

    // create empty dicts / lists instead of null entries
    if (ret.doubleGames     == null) ret.doubleGames     = [];
    if (ret.singleGames     == null) ret.singleGames     = [];
    if (ret.allGames        == null) ret.allGames        = [];
    if (ret.gamesToday      == null) ret.gamesToday      = [];
    if (ret.gamesWeek       == null) ret.gamesWeek       = [];
    if (ret.allPlayers      == null) ret.allPlayers      = {};
    if (ret.playersToday    == null) ret.playersToday    = {};
    if (ret.playersWeek     == null) ret.playersWeek     = {};
    if (ret.rankingToday    == null) ret.rankingToday    = [];
    if (ret.rankingWeek     == null) ret.rankingWeek     = [];
    if (ret.rankingActive   == null) ret.rankingActive   = [];
    // assemble all games from single and double games, if they are missing
    if (ret.allGames == null) {
        ret.allGames = [];
        ret.allGames = ret.allGames.concat(ret.singleGames);
        ret.allGames = ret.allGames.concat(ret.doubleGames);
    }

    // set efficiency and winRatio to 0 if the number of the person's games is too low
    for (const [pName, p] of Object.entries(ret.allPlayers)) {
        if (p.gamesTotal < 5) {
            ret.allPlayers[pName].efficiency = -Infinity;
            ret.allPlayers[pName].winRatio = -Infinity;
        }
    }

    // create list with players which have played at least one game
    ret.playersWithGames = {};
    for (const [pName, p] of Object.entries(ret.allPlayers)) {
        if (p.gamesTotal > 0)
            ret.playersWithGames[pName] = ret.allPlayers[pName];
    }
    // create list with active players (at least one game played and last game not too long ago)
    ret.activePlayers = {};
    for (const [pName, p] of Object.entries(ret.allPlayers)) {
        if (p.isActive)
            ret.activePlayers[pName] = ret.allPlayers[pName];
    }

    ret.nGames            = Object.keys(ret.allGames).length;
    ret.nDoubleGames      = Object.keys(ret.doubleGames).length;
    ret.nSingleGames      = Object.keys(ret.singleGames).length;
    ret.nGames            = Object.keys(ret.allGames).length;
    ret.nGamesToday       = Object.keys(ret.gamesToday).length;
    ret.nGamesWeek        = Object.keys(ret.gamesWeek).length;
    ret.nAllPlayers       = Object.keys(ret.allPlayers).length;
    ret.nPlayersToday     = Object.keys(ret.playersToday).length;
    ret.nPlayersWeek      = Object.keys(ret.playersWeek).length;
    ret.nRankingToday     = Object.keys(ret.rankingToday).length;
    ret.nRankingWeek      = Object.keys(ret.rankingWeek).length;
    ret.nrankingActive    = Object.keys(ret.rankingActive).length;

    ret.nActivePlayers    = Object.keys(ret.activePlayers).length;
    ret.nPlayersWithGames = Object.keys(ret.playersWithGames).length;

    return ret;
}

// Fetches all games by the given player.
async function getPlayerGames(ctx, pName) {
    let res = await fetch(ctx.baseUrl + "/api.php?op=get_player_games&player=" + pName);
    if (!res.ok) throw Error(res.status + res.statusText);
    return await res.json();
}

// Fetches what elo a game with this config would lead to.
async function getEloPreview(ctx, pA1Name, pA2Name, pB1Name, pB2Name, scoreA, scoreB) {
    let res = await fetch(ctx.baseUrl + '/api.php?op=get_game_elo' +
        `&playerA1=${pA1Name}&playerA2=${pA2Name}&playerB1=${pB1Name}&playerB2=${pB2Name}` +
        `&scoreA=${scoreA}&scoreB=${scoreB}`
    );
    if (!res.ok) throw Error(res.status + res.statusText);
    return await res.json();
}



// Adds a td or th item to the given table row. If head is true, th items are created.
// If text is null, the td has no content.
function addCell(tr, txt=undefined, head=false, cssClass=undefined) {
    let td;
    if (head)
        td = document.createElement('TH');
    else
        td = document.createElement('TD');
    if (txt !== undefined)
        td.appendChild(document.createTextNode(txt));
    if (cssClass !== undefined)
        td.className = cssClass;
    tr.appendChild(td)

    return td;
}

// Sorts the given table according to the given column.
// If numeric is false, an alphabetical sort is performed.
function sortTable({tbl, column, numeric=false, ascending=true}) {
    // Get the elements to compare
    let rows = tbl.rows;
    let rowVals = [""]; // insert a dummy value for the header (it's a TH element, so we need this extra handling)
    let indxs = [0];
    for (let r = 1; r < rows.length; r++) {
        let rowVal = rows[r].getElementsByTagName("TD")[column].innerHTML.toLowerCase();
        if (numeric) {
            rowVal = Number(rowVal);
            if(isNaN(rowVal))
                if (ascending)
                    rowVal = Infinity;
                else
                    rowVal = -Infinity;
        }
        rowVals.push(rowVal);
        indxs.push(r);
    }

    // loop until nothing changed in last iteration
    let didSwitch = true;
    while (didSwitch) {
        didSwitch = false;
        // Loop through all table rows (except the first, which contains table headers)
        for (let r = 1; r < (rows.length - 1); r++) {
            // Get the two elements to compare one from current row and one from the next
            let x = rowVals[r];
            let y = rowVals[r + 1];
            // Check if the two rows should switch place
            if (
              (  ascending && (x > y) )
              ||
              ( !ascending && (x < y) )
            ) {
                [indxs[r+1], indxs[r]] = [indxs[r], indxs[r+1]];
                [rowVals[r+1], rowVals[r]] = [rowVals[r], rowVals[r+1]];
                didSwitch = true;
                break;
            }
        }
    }

    // clear the table and refill in right order
    let newRows = [];
    for (let i = 0; i < rows.length; i++)
        newRows.push(rows[indxs[i]]);
    while (tbl.rows.length > 0)
        tbl.deleteRow(0);
    for (let i = 0; i < newRows.length; i++)
        tbl.appendChild(newRows[i]);
}

// Highlights best and worst cells in the given table.
// If there are multiple cells with the same extremal value, they are all highlighted.
// The lims argument limits which region is processed. c/rMin and c/rMax are the first and last columns/rows which are processed.
// You can use infinity as well.
// If invert is true, the colors of best and worst cells are interchanged.
// cnvFun can be a function, which converts cell entries to numbers. E.g.:
// cnvFun = function(inp) {
//     let m = /([\d\.]+)%/.exec(inp);
//     if (m)
//         return Number(m[1]);
//     else
//         return undefined;
// }
// ignore{Min,Max} can be set to true if no highlighting for minimum/maximum values should happen.
function highlightMinMax({
    tbl,
    lims={cMin: 0, cMax: Infinity, rMin: 0, rMax: Infinity},
    invert=false,
    cnvFun=function(inp){return Number(inp);},
    ignoreMin=false,
    ignoreMax=false
}) {
    let minVal = Infinity, maxVal = -Infinity;
    let minCells = [];
    let maxCells = [];
    for (let r = Math.max(0, lims.rMin); r <= Math.min(tbl.rows.length-1, lims.rMax); r++) {
        for (let c = Math.max(0, lims.cMin); c <= Math.min(tbl.rows[0].cells.length-1, lims.cMax); c++) {
            let val = cnvFun(tbl.rows[r].cells[c].innerText);
            if (val === minVal) {
                minCells.push([r, c]);
                minVal = val;
            }
            else if (val < minVal) {
                minCells = [ [r, c] ];
                minVal = val;
            }
            if (val === maxVal) {
                maxCells.push([r, c]);
                maxVal = val;
            }
            if (val > maxVal) {
                maxCells = [ [r, c] ];
                maxVal = val;
            }
        }
    }

    let minStyle = "tdMinScore", maxStyle = "tdMaxScore";
    if (invert) [maxStyle, minStyle] = [minStyle, maxStyle];
    for(let c of minCells)
        if (!ignoreMin)
            tbl.rows[c[0]].cells[c[1]].className = minStyle;
    for(let c of maxCells)
        if (!ignoreMax)
            tbl.rows[c[0]].cells[c[1]].className = maxStyle;
}

// Similar to highlightMinMax, but highlights cells with a linear interpolation
// of two colors, according to the values of the cell w.r.t. minVal and maxVal.
// {min,max}ValOverride: smallest and biggest expected number for this table column. If not given, the smallest and biggest numbers found in the data are used.
// tbl, lims and cnvFun: like in highlightMinMax
function highlightSpectrum({
    tbl,
    minValOverride=null,
    maxValOverride=null,
    highlightMin=-Infinity,
    highlightMax=Infinity,
    lims={cMin: 0, cMax: Infinity, rMin: 0, rMax: Infinity},
    cnvFun=function(inp){return Number(inp);}
}) {
    // get min and max colors by using the table as dummy
    prevClass = tbl.className;
    tbl.className = "tdMinSpectral"; minColor = window.getComputedStyle(tbl).backgroundColor;
    tbl.className = "tdMidSpectral"; midColor = window.getComputedStyle(tbl).backgroundColor;
    tbl.className = "tdMaxSpectral"; maxColor = window.getComputedStyle(tbl).backgroundColor;
    tbl.className = prevClass;
    // convert colors to hsl
    minColorPts = minColor.match(/(\d+),\s*(\d+),\s*(\d+)/);
    minColorHsl = rgbToHsl(minColorPts[1], minColorPts[2], minColorPts[3]);
    midColorPts = midColor.match(/(\d+),\s*(\d+),\s*(\d+)/);
    midColorHsl = rgbToHsl(midColorPts[1], midColorPts[2], midColorPts[3]);
    maxColorPts = maxColor.match(/(\d+),\s*(\d+),\s*(\d+)/);
    maxColorHsl = rgbToHsl(maxColorPts[1], maxColorPts[2], maxColorPts[3]);

    // find smallest and biggest numbers, if at least one of them was not given
    let minVal = Infinity;
    let maxVal = -Infinity;
    if (minValOverride == null || maxValOverride == null) {
        for (let r = Math.max(0, lims.rMin); r <= Math.min(tbl.rows.length-1, lims.rMax); r++) {
            for (let c = Math.max(0, lims.cMin); c <= Math.min(tbl.rows[0].cells.length-1, lims.cMax); c++) {
                let cell = tbl.rows[r].cells[c];
                let val = cnvFun(cell.innerText);
                if (val < minVal) minVal = val;
                if (val > maxVal) maxVal = val;
            }
        }
    }
    // override if given
    if (minValOverride != null) minVal = minValOverride;
    if (maxValOverride != null) maxVal = maxValOverride;

    // if there is only one value in the table, we can't highlight anything
    if (minVal == maxVal)
        return;


    // set cell colors
    for (let r = Math.max(0, lims.rMin); r <= Math.min(tbl.rows.length-1, lims.rMax); r++) {
        for (let c = Math.max(0, lims.cMin); c <= Math.min(tbl.rows[0].cells.length-1, lims.cMax); c++) {
            let cell = tbl.rows[r].cells[c];
            let val = cnvFun(cell.innerText);

            if (val >= highlightMin && val <= highlightMax) {
                // bound val to min/max values
                val = Math.min(maxVal, val);
                val = Math.max(minVal, val);

                // compute new color
                // start with the ratio where val lies compared to min/max values (0..1)
                let colorRatio = (val - minVal) / (maxVal - minVal);
                // set colors and ratio depending on whether we are in 0..0.5 or 0.5..1
                if (colorRatio < 0.5) {
                    var colA = minColorHsl;
                    var colB = midColorHsl;
                    colorRatio *= 2; // expand 0..0.5 to 0..1
                } else {
                    var colA = midColorHsl;
                    var colB = maxColorHsl;
                    colorRatio = colorRatio * 2 - 1; // map 0.5..1 to 0..1
                }
                // get the color
                let h = colA.h + (colB.h - colA.h) * colorRatio;
                let s = colA.s + (colB.s - colA.s) * colorRatio;
                let l = colA.l + (colB.l - colA.l) * colorRatio;

                // set the color and css class
                cell.style.background = "hsl(" + Math.round(h) + "," + Math.round(s) + "%," + Math.round(l) + "%)";
                cell.classList.add("tdSpectralText");
                // inverted font color
                cell.style.color = "hsl(" + Math.round((h+180)%360) + "," + Math.round(s) + "%," + Math.round(l) + "%)";
            }
        }
    }
}

// Highlights a cell if its content matches the given regex.
// The lims argument limits which region is processed. c/rMin and c/rMax are the first and last columns/rows which are processed.
// You can use infinity as well.
function highlightRegex({
    tbl,
    patternMatch="",
    lims={cMin: 0, cMax: Infinity, rMin: 0, rMax: Infinity},
}) {
    let mtchObj = RegExp(patternMatch);
    for (let r = Math.max(0, lims.rMin); r <= Math.min(tbl.rows.length-1, lims.rMax); r++) {
        for (let c = Math.max(0, lims.cMin); c <= Math.min(tbl.rows[0].cells.length-1, lims.cMax); c++) {
            if (mtchObj.test(tbl.rows[r].cells[c].innerText))
                tbl.rows[r].cells[c].className = "tdHighlightMatch";
        }
    }

}


// Deletes all rows of tbl and fills it with all games in "games".
// tbl: The table to fill.
// games: The list of played games.
// eloHighlightFun: A function which can be used to highlight the elo column. Should take a game object and return the CSS class name for the elo cell of that game.
//                  Pass '()=>""' to deactivate it (withouth the ''s)
// gameIdColumn: Whether to add a column with the game id
// ignoredColumn: Whether to add a column with info whether the game is ignored
// decorate: whether to add images for no goals, few goals and high score games
function gamesTable(
    tbl,
    games,
    config,
    eloHighlightFun=function(g){
        if (g.scoreA > g.scoreB)
            return "tdHighlightWon";
        else
            return "tdHighlightLost";
    },
    gameIdColumn=false,
    ignoredColumn=false,
    decorate=true,
){
    // hide table to disable rendering while being built
    let preHidden = tbl.hidden; tbl.hidden = true;

    while (tbl.rows.length > 0)
        tbl.deleteRow(0);

    let tr = document.createElement("tr");
    if (gameIdColumn) addCell(tr, "Game ID", true);
    if (ignoredColumn) addCell(tr, "Ignored", true);
    let cellTime = addCell(tr, "Time", true);
    addCell(tr, "Attack A",  true);
    addCell(tr, "Defense A", true);
    addCell(tr, "Score A",   true);
    addCell(tr, "Score B",   true);
    addCell(tr, "Attack B",  true);
    addCell(tr, "Defense B", true);
    addCell(tr, "Comment",   true);
    let cellElo    = addCell(tr, "Élő Team A", true);
    let cellEloInv = addCell(tr, "Élő Team A if Goals were Swapped", true);
    cellEloInv.title = "In case the teams would have scored exactly the opposite number of goals.";
    addCell(tr, "Max Élő", true);
    tbl.appendChild(tr);

    cellTime.onclick     = function() { sortTable({tbl: tbl, column: 0, numeric: false, ascending: false}); };
    cellElo.onclick      = function() { sortTable({tbl: tbl, column: 8, numeric: true,  ascending: false}); };
    cellEloInv.onclick   = function() { sortTable({tbl: tbl, column: 9, numeric: true,  ascending: false}); };
    cellTime.className   = "monocolorLink";
    cellElo.className    = "monocolorLink";
    cellEloInv.className = "monocolorLink";

    for (let g of games) {
        let tr = document.createElement("tr");
        if (gameIdColumn) addCell(tr, g.id);
        if (ignoredColumn) addCell(tr, g.ignored ? "YES" : "no");
        addCell(tr, moment.utc(g.playTime).local().format('YYYY-MM-DD HH:mm:ss'));
        addCell(tr, capName(g.playerA1));
        addCell(tr, capName(g.playerA2));
        var td = addCell(tr, g.scoreA);
        // add decoration images for bad scores
        if (decorate) {
            if (g.scoreA == 0)
                td.innerHTML += ' <img class="tdInlineImage" onclick="window.open(\'pics/brokenHeart.png\', \'_blank\');" src="pics/brokenHeart.png"/>';
            else if (g.scoreB - g.scoreA == 5)
                td.innerHTML += ' <img class="tdInlineImage" onclick="window.open(\'pics/twilight.png\', \'_blank\');" src="pics/twilight.png"/>';
            else if (g.scoreB - g.scoreA > 5)
                td.innerHTML += ' <img class="tdInlineImage" onclick="window.open(\'pics/noDinner.png\', \'_blank\');" src="pics/noDinner.png"/>';
        }
        var td = addCell(tr, g.scoreB);
        if (decorate) {
            if (g.scoreB == 0)
                td.innerHTML += ' <img class="tdInlineImage" onclick="window.open(\'pics/brokenHeart.png\', \'_blank\');" src="pics/brokenHeart.png"/>';
            else if (g.scoreA - g.scoreB == 5)
                td.innerHTML += ' <img class="tdInlineImage" onclick="window.open(\'pics/twilight.png\', \'_blank\');" src="pics/twilight.png"/>';
            else if (g.scoreA - g.scoreB > 5)
                td.innerHTML += ' <img class="tdInlineImage" onclick="window.open(\'pics/noDinner.png\', \'_blank\');" src="pics/noDinner.png"/>';
        }
        addCell(tr, capName(g.playerB1));
        addCell(tr, capName(g.playerB2));
        addCell(tr, g.comment);
        var td = addCell(tr, g.elo.toFixed(1), false, eloHighlightFun(g));
        // add decoration image for high score games
        if (decorate && g.elo > 0.5 * config.eloKRegular) {
            td.innerHTML += ' <img class="tdInlineImage" onclick="window.open(\'pics/highscore.png\', \'_blank\');" src="pics/highscore.png"/>';
        }
        addCell(tr, g.eloInv.toFixed(1));
        addCell(tr, g.eloK.toFixed(1));

        tbl.appendChild(tr);
    }

    tbl.hidden = preHidden;
}
