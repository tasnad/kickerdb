<?php
define("WEBSITE_PHP_DEF", true);
require_once("common.php");
checkAccess();
setCookiesFromUrl();
require_once("apiFunctions.php");
?>

<!DOCTYPE html>
<html lang="en-US">

<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
<link rel="shortcut icon" type="image/x-icon" href="pics/favicon.ico" />

<link rel="stylesheet" href="css/main.css">
<link rel="stylesheet" href="css/fancySelect.css">
<?php loadTheme() ?>

<script src="lib/kicker.js"></script>
<script src="lib/fancySelect.js"></script>
<script src="lib/moment.js"></script>
</head>



<body>
<menu id="topnav"> </menu>

<div class="flex-container">
    <div> <h1>Select the Player</h1> </div>
    <div id="playerSelect"></div>

    <div> <h1 id=mainTitle></h1> </div>
    <div> <table class="dataTable" id="tblGames"></table> </div>
</div>



<script defer>
setupTopNav();

function setupPlayerSelector(ctx, data) {
    // find the player from cookie or url
    let selPlayerName;
    if ("player" in ctx.args)
        selPlayerName = ctx.args.player;
    else
        selPlayerName = getCookie("player");

    fancySelect.create(document.getElementById("playerSelect"), Object.keys(data.allPlayers).map(x => capName(x)));
    fancySelect.setOnSelect(document.getElementById("playerSelect"), function() {updatePage(glblVars.ctx, glblVars.data)});
    if (!fancySelect.selectByText(document.getElementById("playerSelect"), capName(selPlayerName)))
        fancySelect.selectRandom(document.getElementById("playerSelect"));
}



// updates the page according to the selected player.
// Needs the global glblVars to be set up.
function updatePage(ctx, data) {

    // find the player from cookie or url
    let mainTitle     = document.getElementById("mainTitle");
    let tblGames      = document.getElementById("tblGames");
    let selPlayerName = fancySelect.get(document.getElementById("playerSelect")).toLowerCase();

    if (data.allPlayers[selPlayerName].gamesTotal > 0) {
        mainTitle.innerText = "Games for " + capName(selPlayerName);
        // fetch games by this player and fill the table
        getPlayerGames(ctx, selPlayerName).then( function(pGames) {
            eloHighlightFun = function(g) {
                let plInA = false; if (selPlayerName == g.playerA1 || selPlayerName == g.playerA2) plInA = true;
                let aWon  = false; if (g.scoreA > g.scoreB) aWon  = true;
                if ( (plInA && aWon) || (!plInA && !aWon))
                    return "tdHighlightWon";
                else
                    return "tdHighlightLost";
            }
            gamesTable(tblGames, pGames, glblVars.data.config, eloHighlightFun);
        });
        tblGames.style.display = "";
    } else {
        mainTitle.innerText = "This Player has not played any Games";
        tblGames.style.display = "none";
    }
}



// set up the page
window.onload = function() {
    fetchData(["config", "allPlayers"]).then( function(data) {
        // store data globally
        glblVars = {ctx: getCtx(), data: data};

        setupPlayerSelector(glblVars.ctx, data);
        updatePage(glblVars.ctx, data);
    });
}
</script>
</body>
</html>
