<?php
define("WEBSITE_PHP_DEF", true);
require_once("common.php");
checkAccess();
setCookiesFromUrl();
require_once("apiFunctions.php");
?>

<!DOCTYPE html>
<html lang="en-US">

<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
<link rel="shortcut icon" type="image/x-icon" href="pics/favicon.ico" />

<link rel="stylesheet" href="css/main.css">
<link rel="stylesheet" href="css/fancySelect.css">
<?php loadTheme() ?>

<script src="lib/kicker.js"></script>
<script src="lib/fancySelect.js"></script>

<!-- load chart.js with all related libs -->
<script src="lib/moment.js"></script>
<script src="lib/moment-de.js"></script>
<script src="lib/Chart.js"></script>
<script src="lib/hammer.js"></script>
<script src="lib/chartjs-plugin-zoom.js"></script>
<script src="lib/historyCanvas.js"></script>
</head>



<body>
<menu id="topnav"> </menu>

<div class="flex-container">
    <div> <h1>Select the Player</h1> </div>
    <div id="playerSelect"></div>

    <div> <h1 id=noStatsTxt>No Stats for This Player.</h1> </div>
</div>

<!-- use a second div so that we can hide it easily -->
<div class="flex-container" id="dataDiv">
    <div> <h2>Penalties due to Inactivity</h2> </div>
    <div> <h3 id="noPenalties">No Penalties.</h3> </div>
    <div> <table class="dataTable" id="tblPenalties"></table> </div>

    <div> <h2>Élö History</h2> </div>
    <div> <button onclick="historyCanvas.zoomAll(document.getElementById('eloHistCanvas'));">Zoom Out</button> </div>
    <div> <button onclick="historyCanvas.zoomLastMonth(document.getElementById('eloHistCanvas'));">Zoom Last Month</button> </div>
    <div> <canvas id="eloHistCanvas"></canvas> </div>

    <div> <h2>Teammate in Row, Opponent in Column</h2> </div>
    <div> <table class="dataTable" id="tblFriendFoe"></table> </div>

    <div> <h2>Opponents in Row and Column</h2> </div>
    <div> <table class="dataTable" id="tblOpponent"></table> </div>

    <div> <h2>Opponent's Attack in Row, Defense in Column</h2> </div>
    <div> <table class="dataTable" id="tblOponentRC"></table> </div>
</div>



<script defer>
function setupPlayerSelector(ctx, data) {
    // find the player from cookie or url
    let selPlayerName;
    if ("player" in ctx.args)
        selPlayerName = ctx.args.player;
    else
        selPlayerName = getCookie("player");

    // use all players and just display an info if the selected player has no games
    fancySelect.create(document.getElementById("playerSelect"), Object.keys(data.allPlayers).map(x => capName(x)));
    fancySelect.setOnSelect(document.getElementById("playerSelect"), updatePage);
    if (!fancySelect.selectByText(document.getElementById("playerSelect"), capName(selPlayerName)))
        fancySelect.selectRandom(document.getElementById("playerSelect"));
}



// Populates the tables from game stats, depending on user settings.
// selPlayerName: Generate tables etc. for this player. Make sure it's lower case.
function genTables(data, selPlayerName) {
    // increments the value in the cell defined by row r and column c in table tbl by the given ammount inc.
    function addTo(tbl, r, c, inc) {
        let e = tbl.rows[r].cells[c];
        if (e.innerText === "-")
            e.innerText = 0;
        e.innerText = Number(e.innerText) + inc;
    }

    let tblPenalties = document.getElementById("tblPenalties");
    let tblFriendFoe = document.getElementById("tblFriendFoe");
    let tblOpponent  = document.getElementById("tblOpponent");
    let tblOponentRC = document.getElementById("tblOponentRC");
    let noPenalties  = document.getElementById("noPenalties");

    // hide for performance
    tblPenalties.hidden = true;
    tblFriendFoe.hidden = true;
    tblOpponent.hidden  = true;
    tblOponentRC.hidden = true;

    // clear tables
    while (tblPenalties.rows.length > 0) tblPenalties.deleteRow(0);
    while (tblFriendFoe.rows.length > 0) tblFriendFoe.deleteRow(0);
    while (tblOpponent.rows.length  > 0) tblOpponent.deleteRow(0);
    while (tblOponentRC.rows.length > 0) tblOponentRC.deleteRow(0);

    // we can't properly display guests. They need extra considerations, since they can be in both teams, etc.
    // Since displaying stats for dummy users does not make much sense anyways, just don't do it.
    if (data.playersWithGames[selPlayerName].playerType == <?php print(PlayerType::Dummy) ?>) {
        noPenalties.hidden = false;
    }
    else {
        // create table with penalties
        let pens = data.allPlayers[selPlayerName].eloPenalties;
        if (pens.length == 0) {
            noPenalties.hidden = false;
        }
        else {
            pens = JSON.parse(pens);

            let penaltyDelayDays    = <?php print(ELO_PENALTY_DELAY_DAYS) ?>;
            let penaltyPerDay       = <?php print(ELO_PENALTY_PER_DAY) ?>;
            let penaltyExponent     = <?php print(ELO_PENALTY_EXPONENT) ?>;
            let penaltyActiveRange  = <?php print(ELO_PENALTY_ACTIVE_RANGE) ?>;
            let penaltyTypeActive   = <?php print(EloPenaltyType::Active) ?>;
            let penaltyTypeDecaying = <?php print(EloPenaltyType::Decaying) ?>;
            let penaltyDecayRate    = <?php print(ELO_PENALTY_DECAY_FRACT_PER_DAY) ?>;

            noPenalties.hidden = true;

            let th = document.createElement("tr");
            addCell(th, "Inactive From", true);
            addCell(th, "Inactive Until", true);
            addCell(th, "Gap [days]", true);
            let cellPenFull    = addCell(th, "Full Penalty", true);
            cellPenFull.title = `Penalty is [(gap - ${penaltyDelayDays}) * ${penaltyPerDay.toFixed(2)}]^${penaltyExponent}`;
            let cellPenDecayed = addCell(th, "Effective Penalty", true);
            cellPenDecayed.title = `Penalties start to decay ${penaltyActiveRange} days after the gap's end, by ${penaltyDecayRate*100}% per day.`;
            tblPenalties.appendChild(th);

            var gapTotal = 0, penPreDecTotal = 0, penTotal = 0;
            for (let pen of pens.reverse()) {
                let tr = document.createElement("tr");
                let from = moment.utc(pen.from);
                let to   = moment.utc(pen.to);

                let thisGap = to.diff(from, "seconds") / 86400;
                let thisPenPreDec = pen.valuePreDec;
                let thisPen = pen.value;

                addCell(tr, from.local().format('DD. MMM YYYY, HH:mm:ss'));
                addCell(tr,   to.local().format('DD. MMM YYYY, HH:mm:ss'));
                addCell(tr, thisGap.toFixed(1));
                addCell(tr, thisPenPreDec.toFixed(1));
                addCell(tr, thisPen.toFixed(1));
                tblPenalties.appendChild(tr);

                gapTotal += thisGap;
                penPreDecTotal += thisPenPreDec;
                penTotal += thisPen;
            }
        }
        if (pens.length > 0) {
            let tr = document.createElement("tr");
            addCell(tr, "", true);
            addCell(tr, "Totals:", true);
            addCell(tr, gapTotal.toFixed(1), true);
            addCell(tr, penPreDecTotal.toFixed(1), true);
            addCell(tr, penTotal.toFixed(1), true);
            tblPenalties.appendChild(tr);

            tblPenalties.hidden = false;
        }



        // create table with teammate in row and opponent in column
        let plName2TblR = {}; // player name -> table row index dict
        let plName2TblC = {}; // player name -> table column index dict
        let r = 1;
        let cols = {player: 0, total: data.nPlayersWithGames}; // column indices

        th = document.createElement("tr");
        let cellPlayer = addCell(th, capName(selPlayerName), true);
        cellPlayer.onclick = function() { sortTable({tbl: tblFriendFoe, column: cols.player, numeric: false, ascending: true}) };
        cellPlayer.className = "monocolorLink";
        for (let [pName, p] of Object.entries(data.playersWithGames)) {
            plName2TblR[p.name] = r++;
            // header
            if (pName !== selPlayerName) {
                addCell(th, capName(pName), true);
                plName2TblC[p.name] = th.cells.length - 1;
            }
            // higher rows
            let tr = document.createElement("tr");
            addCell(tr, capName(pName));
            for (let c = 0; c < data.nPlayersWithGames-1+1; c++)
                addCell(tr, "-");
            tblFriendFoe.appendChild(tr);
        }
        let cellTotal = addCell(th, "Total", true);
        cellTotal.onclick = function() { sortTable({tbl: tblFriendFoe, column: cols.total, numeric: true, ascending: false}) };
        cellTotal.title = "Total goal difference of all games";
        cellTotal.className = "monocolorLink";
        tblFriendFoe.insertBefore(th, tblFriendFoe.rows[0]);

        // add scores
        for (let g of data.allGames) {
            let a1 = {name: g.playerA1, r: plName2TblR[g.playerA1], c: plName2TblC[g.playerA1]},
                a2 = {name: g.playerA2, r: plName2TblR[g.playerA2], c: plName2TblC[g.playerA2]},
                b1 = {name: g.playerB1, r: plName2TblR[g.playerB1], c: plName2TblC[g.playerB1]},
                b2 = {name: g.playerB2, r: plName2TblR[g.playerB2], c: plName2TblC[g.playerB2]};
            let s = g.scoreA - g.scoreB;

            // filter for games selPlayerName was involved
            if (a1.name === selPlayerName) {
                addTo(tblFriendFoe, a2.r, b1.c, s);
                if (b1.c !== b2.c) // make sure to only add 1vs2 games once
                    addTo(tblFriendFoe, a2.r, b2.c, s);
                addTo(tblFriendFoe, a2.r, cols.total, s); // add to total, but only once per game
            }
            else if (a2.name === selPlayerName) {
                addTo(tblFriendFoe, a1.r, b1.c, s);
                if (b1.c !== b2.c)
                    addTo(tblFriendFoe, a1.r, b2.c, s);
                addTo(tblFriendFoe, a1.r, cols.total, s);
            }
            else if (b1.name === selPlayerName) {
                addTo(tblFriendFoe, b2.r, a1.c, -s);
                if (a1.c !== a2.c)
                    addTo(tblFriendFoe, b2.r, a2.c, -s);
                addTo(tblFriendFoe, b2.r, cols.total, -s);
            }
            else if (b2.name === selPlayerName) {
                addTo(tblFriendFoe, b1.r, a1.c, -s);
                if (a1.c !== a2.c)
                    addTo(tblFriendFoe, b1.r, a2.c, -s);
                addTo(tblFriendFoe, b1.r, cols.total, -s);
            }
        }

        highlightSpectrum({tbl: tblFriendFoe, lims: {cMin: 1, cMax: cols.total-1, rMin: 1, rMax: Infinity}});
        highlightSpectrum({tbl: tblFriendFoe, lims: {cMin: cols.total, cMax: cols.total, rMin: 1, rMax: Infinity}});
        tblFriendFoe.hidden = false;



        // create table with opponents in rows and columns
        let plName2TblRC = {}; // player name -> table row/column index dict
        cols = {player: 0, total: data.nPlayersWithGames}; // column indices

        th = document.createElement("tr");
        cellPlayer = addCell(th, capName(selPlayerName), true);
        cellPlayer.onclick = function() { sortTable({tbl: tblOpponent, column: cols.player, numeric: false, ascending: true}) };
        cellPlayer.className = "monocolorLink";
        for (let [pName, p] of Object.entries(data.playersWithGames)) {
            if (pName !== selPlayerName) {
                // header
                addCell(th, capName(pName), true);
                plName2TblRC[p.name] = th.cells.length - 1;

                // higher rows
                let tr = document.createElement("tr");
                addCell(tr, capName(pName));
                for (let c = 0; c < data.nPlayersWithGames-1+1; c++)
                    addCell(tr, "-");
                tblOpponent.appendChild(tr);
            }
        }
        cellTotal = addCell(th, "Total", true);
        cellTotal.onclick = function() { sortTable({tbl: tblOpponent, column: cols.total, numeric: true, ascending: false}) };
        cellTotal.title = "Total goal difference of all games";
        cellTotal.className = "monocolorLink";
        tblOpponent.insertBefore(th, tblOpponent.rows[0]);

        // add scores
        for (let g of data.allGames) {
            let a1 = {name: g.playerA1, indx: plName2TblRC[g.playerA1]},
                a2 = {name: g.playerA2, indx: plName2TblRC[g.playerA2]},
                b1 = {name: g.playerB1, indx: plName2TblRC[g.playerB1]},
                b2 = {name: g.playerB2, indx: plName2TblRC[g.playerB2]};
            let s = g.scoreA - g.scoreB;

            // filter for games selPlayerName was involved
            if (a1.name === selPlayerName || a2.name === selPlayerName) {
                let r = Math.max(b1.indx, b2.indx);
                let c = Math.min(b1.indx, b2.indx);
                addTo(tblOpponent, r, c, s);
                addTo(tblOpponent, r, cols.total, s);
            }
            else if (b1.name === selPlayerName || b2.name === selPlayerName) {
                let r = Math.max(a1.indx, a2.indx);
                let c = Math.min(a1.indx, a2.indx);
                addTo(tblOpponent, r, c, -s);
                addTo(tblOpponent, r, cols.total, -s);
            }
        }

        highlightSpectrum({tbl: tblOpponent, lims: {cMin: 1, cMax: cols.total-1, rMin: 1, rMax: Infinity}});
        highlightSpectrum({tbl: tblOpponent, lims: {cMin: cols.total, cMax: cols.total, rMin: 1, rMax: Infinity}});
        tblOpponent.hidden = false;




        // create table with opponent attack in row and defense in column
        plName2TblRC = {}; // player name -> table row/column index dict
        cols = {player: 0, total: data.nPlayersWithGames}; // column indices

        th = document.createElement("tr");
        cellPlayer = addCell(th, capName(selPlayerName), true);
        cellPlayer.onclick = function() { sortTable({tbl: tblOponentRC, column: cols.player, numeric: false, ascending: true}) };
        cellPlayer.className = "monocolorLink";
        for (let [pName, p] of Object.entries(data.playersWithGames)) {
            if (pName !== selPlayerName) {
                // header
                addCell(th, capName(pName), true);
                plName2TblRC[p.name] = th.cells.length - 1;

                // higher rows
                let tr = document.createElement("tr");
                addCell(tr, capName(pName));
                for (let c = 0; c < data.nPlayersWithGames-1+1; c++)
                    addCell(tr, "-");
                tblOponentRC.appendChild(tr);
            }
        }
        cellTotal = addCell(th, "Total", true);
        cellTotal.onclick = function() { sortTable({tbl: tblOponentRC, column: cols.total, numeric: true, ascending: false}) };
        cellTotal.title = "Total goal difference of all games";
        cellTotal.className = "monocolorLink";
        tblOponentRC.insertBefore(th, tblOponentRC.rows[0]);

        // add scores
        for (i = 0; i < data.nGames; i++) {
            let g = data.allGames[i];
            let a1 = {name: g.playerA1, indx: plName2TblRC[g.playerA1]},
                a2 = {name: g.playerA2, indx: plName2TblRC[g.playerA2]},
                b1 = {name: g.playerB1, indx: plName2TblRC[g.playerB1]},
                b2 = {name: g.playerB2, indx: plName2TblRC[g.playerB2]};
            let s = g.scoreA - g.scoreB;

            // filter for games selPlayerName was involved
            if (a1.name === selPlayerName || a2.name === selPlayerName) {
                addTo(tblOponentRC, b1.indx, b2.indx, s);
                addTo(tblOponentRC, b1.indx, cols.total, s);
            }
            else if (b1.name === selPlayerName || b2.name === selPlayerName) {
                addTo(tblOponentRC, a1.indx, a2.indx, -s);
                addTo(tblOponentRC, a1.indx, cols.total, -s);
            }
        }

        highlightSpectrum({tbl: tblOponentRC, lims: {cMin: 1, cMax: cols.total-1, rMin: 1, rMax: Infinity}});
        highlightSpectrum({tbl: tblOponentRC, lims: {cMin: cols.total, cMax: cols.total, rMin: 1, rMax: Infinity}});
        tblOponentRC.hidden = false;
    }
}



// updates all page sections according to the selected player.
// Needs the global glblData to be set up.
function updatePage() {
    let noStatsTxt = document.getElementById("noStatsTxt");
    let dataDiv    = document.getElementById("dataDiv");

    let selPlayerName = fancySelect.get(document.getElementById("playerSelect")).toLowerCase();
    if (glblData.allPlayers[selPlayerName].gamesTotal == 0) {
        noStatsTxt.hidden = false;
        dataDiv.style.display = "none";
    }
    else {
        noStatsTxt.hidden = true;

        historyCanvas.fillChart(document.getElementById("eloHistCanvas"), [glblData.allPlayers[selPlayerName]]);
        genTables(glblData, selPlayerName);

        dataDiv.style.display = "";
    }
}



// set up the page
window.onload = function() {
    setupTopNav();

    fetchData(["allGames", "allPlayers", "eloDetails"]).then( function(data) {
        // store ctx and data globally
        glblData = data;
        setupPlayerSelector(getCtx(), data);
        // create the chart
        historyCanvas.genChart(document.getElementById("eloHistCanvas"), 1);
        // load data for the selected player
        updatePage();
    });
}
</script>
</body>
</html>
