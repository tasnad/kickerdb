<?php
define("WEBSITE_PHP_DEF", true);
require_once("common.php");
checkAccess();
setCookiesFromUrl();
?>

<!DOCTYPE html>
<html lang="en-US">

<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
<link rel="shortcut icon" type="image/x-icon" href="pics/favicon.ico" />

<link rel="stylesheet" href="css/main.css">
<?php loadTheme() ?>

<script src="lib/kicker.js"></script>
</head>



<body>
<menu id="topnav"> </menu>

<div class="flex-container">
<div><h1> Our Rules in Short </h1></div>
<div><ul>
    <li>At the start of the game, the ball is thrown in from the hole at the side.</li>
    <li>After a goal, the team which did not score gets the ball for the next kickoff.</li>
    <li>If the ball dies (nobody can't reach it any more) between the 5-row and adjoint goal, the goal keeper of that goal gets the ball.</li>
    <li>If the ball dies between the two 5-rows, the last team with kickoff gets it. It is thrown in again from the side, if it was the start of the game.</li>
    <li>If the ball jumps out, the team in defense (so not the one which kicked it out) gets the ball for kickoff on the 5-row.</li>
    <li>During kickoff, the opponent team needs to be ready before the game starts. The rule where a goal with the first touch was not counted is obsolete.</li>
    <li>If the ball is dead, it is forbidden to move it by pounding a bar against the table. The only exception is if it is between the player and the wall: <img style="vertical-align: middle; width: 100px; height:100px;" src="pics/nondeadBall.jpg"/></li>
</ul></div>

<div><h1> Penalties </h1></div>
<div><p id="penaltyP"></p></div>

<div><h1> International Rules </h1></div>
<div><p>
These are the official rules in german from <a href="https://www.tischfussball-online.com/tischfussball-regeln.html">here</a>. They are slightly different when it comes to jumping balls, but otherwise they are the same.
</p></div>

<div><h2>Die Partie</h2></div>
<div><p>
Wenn die Turnierleitung nicht anders entscheidet, besteht eine Partie aus 3 Gewinnsätzen. Jedes Spiel geht bis 5. Wird das fünfte Spiel ausgetragen, benötigt man zum Sieg eine Differenz von 2 Toren, gewinnt aber ungeachtet dessen mit 8 Toren.
</p></div>

<div><h2>Spielaufnahme</h2></div>
<div><p>
Vor Spielbeginn wird eine Münze geworfen. Die Mannschaft, die den Münzwurf gewinnt, darf entweder die Tischseite oder das Recht der ersten Auflage wählen. Die andere Mannschaft erhält damit automatisch die verbleibende Option.
</p></div>

<div><h2>Auflage</h2></div>
<div><p>
Die Auflage erfolgt bei der mittleren Figur der Fünferreihe unter Beachtung der Bereitschaft.
</p></div>

<div><h2>Bereitschaft</h2></div>
<div><p>
Der Ball muss ruhen. Dann fragt der Spieler in Ballbesitz „Fertig?“, der Gegner bestätigt innerhalb von 3 Sekunden mit „Fertig!“. Binnen 3 Sekunden beginnt daraufhin das Spiel. Erfolgt ein Verstoß, wird der Vorgang einmalig wiederholt.

Der Ball muss nun innerhalb der Reihe eine andere Figur berühren und kann innerhalb der Reihe gleich weiter gespielt werden, darf diese jedoch frühestens eine Sekunde nach der Berührung verlassen. Besteht hierbei ein Verstoß, hat der Gegner die Wahl, das Spiel von der aktuellen Position fortzusetzen oder eine eigene Neuauflage vorzunehmen. Eine Sekunde nach der Berührung beginnt das Zeitlimit.
</p></div>

<div><h2>Ball im Aus</h2></div>
<div><p>
Verlässt der Ball das Spielfeld, bringt die Mannschaft, die sich bei der letzten kontrollierten Aktion in der Defensive befand, den Ball unter Beachtung der Bereitschaft im Abwehrbereich zurück ins Spiel.
</p></div>

<div><h2>Toter Ball</h2></div>
<div><p>
Ein Ball wird für tot erklärt, wenn er bewegungslos liegt und von keiner Spielfigur erreicht werden kann. Liegt ein Ball tot zwischen den Fünferreihen, wird er auf der Fünferreihe der Mannschaft ins Spiel gebracht, die zuletzt das Auflagerecht hatte. Liegt ein Ball tot zwischen Tor und Fünferreihe, wird er auf der Zweierreihe, die näher am Ball ist, ins Spiel gebracht. Jeweils muss die Bereitschaft beachtet werden. Ein Ball, der in Ruhe auf der Torwartstange liegt, gilt als tot.
</p></div>

<div><h2>Rundschlagen</h2></div>
<div><p>
Rundschlag, dass heißt die Stange dreht sich vor oder nach Ballberührung um mehr als 360°, ist verboten und zählt als Foul.
</p></div>

<div><h2>Seitenwechsel</h2></div>
<div><p>
Die Mannschaften dürfen am Ende eines Spiels die Seiten wechseln und haben dafür bis zu 90 Sekunden Zeit. Ist einmal ein Wechsel erfolgt, muss auch nach allen weiteren Spielen gewechselt werden.
</p></div>

<div><h2>Time Out</h2></div>
<div><p>
Jedes Team darf pro Spiel zwei Time Outs nehmen. Ein Time Out dauert maximal 30 Sekunden. Ist der Ball im Spiel, darf nur das Team, das im Ballbesitz ist, ein Time Out nehmen, sonst dürfen beide Teams das Time Out nehmen. Bei der Spielaufnahme muss die Bereitschaft beachtet werden.
</p></div>

<div><h2>Erzielte Treffer</h2></div>
<div><p>
Ein Ball, der die Torlinie überscheitet, wird als Tor gewertet, auch wenn er wieder aus dem Tor heraus kommt. Nach einem Tor erhält die Mannschaft das Auflagerecht, gegen die ein Tor erzielt wurde.

Positionswechsel: Nach einem Tor, nach einem Satz und während eines Time Outs dürfen beide Mannschaften die Positionen wechseln.
</p></div>

<div><h2>Passen</h2></div>
<div><p>
Ein gestoppter Ball muss vor dem Pass auf die nächste Reihe (d.h. aus dem Abwehrbereich auf die Fünferreihe bzw. von dieser auf die Dreierreihe) mindestens zwei Figuren berühren. Wird der Ball umgehend nach dem Stoppen gepasst, ist dies zulässig. Bevor ein Pass von der Fünferreihe versucht wird, darf der Ball nicht öfter als zweimal die Bande berührt haben.
</p></div>

<div><h2>Zeitlimit</h2></div>
<div><p>
Maximal 10 Sekunden auf der Fünferreihe und 15 Sekunden auf den anderen Reihen, wobei die Torwart- und Abwehrstange als eine Stange betrachtet werden. Bei einer Zeitüberschreitung auf der Dreierreihe erhält die gegnerische Mannschaft den Ball auf die Zweierreihe, sonst Auflagerecht auf der Fünferreihe.
</p></div>

<div><h2>Anschlagen</h2></div>
<div><p>
Jedes Bewegen, Schieben oder Anheben des Tisches ist verboten. Erster und zweiter Verstoß: der Gegner hat die Wahl zwischen dem Weiterspielen von der aktuellen Position, von da wo der Verstoß auftreten ist oder dem Neuauflegen auf der Fünferreihe. Weitere Verstöße: Technisches Foul.
</p></div>

<div><h2>Heber</h2></div>
<div><p>
Heber („Aerials“) sind nicht erlaubt und führen zum Auflagerecht auf der Fünferreihe für den Gegner.
</p></div>
</div>



<script defer>
window.onload = function() {
    setupTopNav();
    let penaltyDelayDays   = <?php print(ELO_PENALTY_DELAY_DAYS) ?>;
    let penaltyPerDay      = <?php print(ELO_PENALTY_PER_DAY) ?>;
    let penaltyExponent    = <?php print(ELO_PENALTY_EXPONENT) ?>;
    let penaltyActiveRange = <?php print(ELO_PENALTY_ACTIVE_RANGE) ?>;
    let penaltyDecayRate   = <?php print(ELO_PENALTY_DECAY_FRACT_PER_DAY) ?>;
    document.getElementById("penaltyP").innerHTML =
        `If a player stops playing, his score will decrease by time. The current formula is [(gap - ${penaltyDelayDays}) * ${penaltyPerDay.toFixed(2)}]^${penaltyExponent} where gap is measured in days.` +
        "</br>" +
        `Only inactivity periods ending in the last ${penaltyActiveRange} days are considered for penalties. ` +
        `A penalty due to inactivity decays with a rate of ${penaltyDecayRate*100}% per day.`;
}
</script>
</body>
</html>
