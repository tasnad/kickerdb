<?php
define("WEBSITE_PHP_DEF", true);
require_once("common.php");
checkAccess();
setCookiesFromUrl();
require_once("apiFunctions.php");
?>

<!DOCTYPE html>
<html lang="en-US">

<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
<link rel="shortcut icon" type="image/x-icon" href="pics/favicon.ico" />

<link rel="stylesheet" href="css/main.css">
<link rel="stylesheet" href="css/fancySelect.css">
<?php loadTheme() ?>

<script src="lib/kicker.js"></script>
<script src="lib/fancySelect.js"></script>
</head>



<body>
<menu id="topnav"> </menu>

<div class="flex-container">
    <div> <h1>Settings</h1> </div>

    <div> <fieldset class="entryFieldGrid">
        <div class="grid">
            <label for="txtSecret"> Secret </label> <input type="text" id="txtSecret" size="40"> </input>
            <label for="txtAdminToken"> Admin Token </label> <input type="text" id="txtAdminToken" size="40"> </input>
            <label for="themeSelect"> Theme </label> <div id="themeSelect"></div>
            <label for="playerSelect"> You </label> <div id="playerSelect"></div>
        </div>
    </fieldset> </div>

    <div>
        <div> <button id="btnSubmit" onclick="submitSettings()">Submit Settings</button> </div>
        <div> <button id="btnDelete" onclick="deleteSettings()">Delete All Cookies</button> </div>
    </div>
</div>



<script defer>
function submitSettings() {
    let txtSecret      = document.getElementById("txtSecret")
    let txtAdminToken  = document.getElementById("txtAdminToken")
    let selectedTheme  = fancySelect.get(document.getElementById("themeSelect"));
    let selectedPlayer = fancySelect.get(document.getElementById("playerSelect"));
    if (selectedPlayer == "[nobody]") selectedPlayer = "";

    let cookiePath = "<?php print(SUB_PATH); ?>";
    let tld        = "<?php print(SITE_TLD); ?>";
    setCookie("secret",      txtSecret.value, tld, cookiePath);
    setCookie("admin_token", txtAdminToken.value, tld, cookiePath);
    setCookie("theme",       selectedTheme, tld, cookiePath);
    setCookie("player",      selectedPlayer.toLowerCase(), tld, cookiePath);
    location.reload();
}

function deleteSettings() {
    let cookiePath = "<?php print(SUB_PATH); ?>";
    let tld        = "<?php print(SITE_TLD); ?>";
    deleteCookie("secret", tld, cookiePath);
    deleteCookie("admin_token", tld, cookiePath);
    deleteCookie("theme", tld, cookiePath);
    deleteCookie("player", tld, cookiePath);
    window.location = "delCookies.html";
}

window.onload = function() {
    setupTopNav();

    // load secrets
    let txtSecret   = document.getElementById("txtSecret")
    txtSecret.value = getCookie("secret");
    let txtAdminToken   = document.getElementById("txtAdminToken")
    txtAdminToken.value = getCookie("admin_token");

    // create and populate theme selector
    fancySelect.create(document.getElementById("themeSelect"), ["Bright", "Dark"]);
    fancySelect.selectByText(document.getElementById("themeSelect"), getCookie("theme"));

    // create and populate player selector
    let playerNames = JSON.parse( <?php print("'" . json_encode(getPlayersWithGamesNames()) . "'") ?> );
    playerNames.unshift("[nobody]");

    let selectedName = capName(getCookie("player"));
    if (selectedName == "") selectedName = "[nobody]";

    fancySelect.create(document.getElementById("playerSelect"), playerNames.map(x => capName(x)));
    fancySelect.selectByText(document.getElementById("playerSelect"), selectedName);
}
</script>
</body>
</html>
