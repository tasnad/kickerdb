<?php
if(!defined("WEBSITE_PHP_DEF")) {
    header("HTTP/1.1 401 Unauthorized");
    die("Access denied");
}

// access tokens and database password
define("KICKER_SECRET",      'SECRET1');
define("KICKER_ADMIN_TOKEN", 'SECRET2');
define("KICKER_DB_PW",       'SECRET3');

// the toplevel domain where the app is hosted
define("SITE_TLD", "example.com");
// the relative address where the app is hosted (e.g. "kickerdb" for example.com/kickerdb/" or "/" for example.com/)
define("SUB_PATH", "kickerdb");

// database type and path/socket. Supported are mysql and sqlite.
define("DB_TYPE", "mysql"); define("DB_PATH", "/run/mysqld/mysqld.sock");
//define("DB_TYPE", "sqlite"); define("DB_PATH", "/tmp/kicker.sql3");

// which database to use
define("DB_NAME", "kickerdb");
// database user (do not use root, create an own user for this app)
define("DB_USER", "kickerdb");

// which table to read players from
define("PLAYERS_TABLE", "players");
// which table to store games in
define("GAMES_TABLE", "games");

// set the timezone
define("TIMEZONE", "Europe/Berlin");

// only consider games which are not older than defined here.
// This also limits times in most API requests. E.g. getData() will also only report games in this timespan, except if $includeOldGames is true.
//
// There are two possibilities here:
// 1) Period: Set a string which can be fed to a DateInterval object, i.e. it starts with 'P' for period and then has units added, e.g. 1Y for one year
// or 1Y2M for one year and two months.
// 2) Fixed start time: Set a string which represents a fixed date and time, e.g. "2020-05-01 00:00:00". Not that it does not start with "P".
// define("TIME_WINDOW", "0001-01-01 00:00:00");
// define("TIME_WINDOW", "P6M");
define("TIME_WINDOW", "2020-01-01 00:00:00");


// e-mail settings
// set to false to deactivate sending any e-mails
define("SEND_MAILS",      true);
define("MAIL_TX_ADDRESS", "kicker-noreply@" . SITE_TLD);
// define("MAIL_TX_ADDRESS", "user@example.com"); // you probably will have to enter your real e-mail address here, servers which allow changing the sender address are rare
define("MAIL_HEADER",     "[kickerdb] Game entered!");
// SMTP settings, commet them to use sendmail provided by the system
define("SMTP_HOST",       'smtp.example.com');
define("SMTP_PORT",       465);
define("SMTP_USER",       'johndoe');
define("SMTP_PASSWORD",   'john_does_secret_password');


// time after which a player is considered as inactive
// Passed to a DateInterval object.
//define("PLAYER_INACTIVE_TIME", "P100Y");
define("PLAYER_INACTIVE_TIME", "P6M");

// definitions for the Élő point computations
define("ELO_INIT", 1000); // initial Élő points of a player
define("ELO_F",    800); // point difference where a win is less likely than 0.5%
// k factor for fresh regular games (if a newcomer with bonus plays in a game, his personal K overrides this)
define("ELO_K_REGULAR", 100);
// for how many games does a player count as newcomer (altered K)?
define("ELO_NEWCOMER_GAMES", 30);
// settings for computing the Élő "S" parameter
define("GOALS_PER_GAME", 10); // regular number of goals to win a match
// for overly long games, start reducing elo and freeze reduction if this many excess goals were shot
// Set to 0 to deactivate this feature.
define("ELO_EXCESS_GOALS_MAX", 5);
// This parameter is used for considering the goal difference for elo computation.
// 1 is linear and 0 < ELO_S_EXPONENT < 1 makes most sense, like 0.3
// Set to 0 to compute elo without considering the goal difference
define("ELO_S_EXPONENT", 0.3);

// start penalizing players after this ammount of days
define("ELO_PENALTY_DELAY_DAYS", 7);
// how much points a player loses per day (after ELO_PENALTY_DELAY_DAYS)
define("ELO_PENALTY_PER_DAY", 0); // disable penalty
//define("ELO_PENALTY_PER_DAY", ELO_K_REGULAR/4 / 7); // fourth of a perfect game per week
// penalty is nonlinear with this exponent
define("ELO_PENALTY_EXPONENT", 1.0);
// Consider only inactivity gaps which happened between this ammount of days ago and now
define("ELO_PENALTY_ACTIVE_RANGE", 30*1e9); // consider all penalties
// After ELO_PENALTY_ACTIVE_RANGE, the penalty starts decaying with this fraction per day
define("ELO_PENALTY_DECAY_FRACT_PER_DAY", 0.0); // never decay
